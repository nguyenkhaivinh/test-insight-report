﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.Model
{
    public class GetPolicyTransactionsRq
    {
        public bool IsPageCountRequired { set; get; }
        public int PageNumber { set; get; }
        public int RowsPerPage { set; get; }
        public string SearchCriteria { set; get; }
        public string ShowFilter { set; get; }
        public string SortBy { set; get; }
    }
}
