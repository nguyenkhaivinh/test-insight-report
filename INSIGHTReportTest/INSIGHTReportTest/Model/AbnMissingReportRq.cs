﻿
namespace INSIGHTReportTest.Model
{
    public class AbnMissingReportRq
    {
        public bool SelectAllRecords { set; get; }
        public string EntityName { set; get; }
        public string EntityType { set; get; }
        public string FileType { set; get; }
    }
}
