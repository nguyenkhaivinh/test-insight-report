﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.Model
{
    public class InsurerLiabilityReportRq
    {
        public bool SelectAllRecords { set; get; }
        public bool SelectAllPayableDateRecords { set; get; }
        public string FileType { set; get; }
        public string status { set; get; }
        public string AsAtDate { set; get; }
        public string FromDate { set; get; }
        public string ToDate { set; get; }
        public int InsurerID { set; get; }
        public int SalesTeamId { set; get; }
        public int ServiceTeamId { set; get; }
        public int AuthorisedRepId {set; get;}
        public string AuthorisedRepAutoComplete { set; get; }
        public int BranchId { set; get; }
        public int ClassOfRiskId { set; get; }
        public string GroupBy { set; get; }
        public bool GroupPageBreak { set; get; }
        public string SortBy { set; get; }
    }
}
