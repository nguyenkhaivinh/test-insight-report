﻿
namespace INSIGHTReportTest.Report
{
    public class InsurerLiabilityReport : ReportTestBase
    {
        public InsurerLiabilityReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InsurerLiabilityAuditReport";

            reportParameter.SelectAllRecords = true;
            reportParameter.SelectAllPayableDateRecords = true;
            reportParameter.FileType = fileType;
            reportParameter.status = "Payable";
            reportParameter.AsAtDate = "2020-03-26";
            reportParameter.FromDate = "2019-03-01";
            reportParameter.ToDate = "2020-03-26";
            reportParameter.InsurerID = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.AuthorisedRepAutoComplete = "(All)";
            reportParameter.BranchId = 0;
            reportParameter.ClassOfRiskId = 0;
            reportParameter.GroupBy = "Insurer";
            reportParameter.GroupPageBreak = false;
            reportParameter.SortBy = "AuthorisedRep";
        }

    }
}
