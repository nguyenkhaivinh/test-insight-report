﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class ClientFSGEmailListReport : ReportTestBase
    {
        public ClientFSGEmailListReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClientFSGEmailListReport";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.AuthorisedRepAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.BranchId = 0;
            reportParameter.ClientId = 0;
            reportParameter.CreateFSGTask = true;
            reportParameter.ExcludeClientAlreadySent = true;
            reportParameter.SalesTeamId = 17;
            reportParameter.SelectAllRecords = false;
            reportParameter.ServiceTeamId = 0;
        }
    }
}
