﻿namespace INSIGHTReportTest.Report
{
    public class ProcessingPermissionGroupsReport : ReportTestBase
    {

        public ProcessingPermissionGroupsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ProcessingPermissionGroupsReport";
            reportParameter.FileType = fileType;

        }
    }
}
