﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class AuthorisedRepLiabilityReport : ReportTestBase
    {
        public AuthorisedRepLiabilityReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ARLiabilityAuditReport";
            reportParameter.AsAtDate = "2018-06-01";
            reportParameter.FileType = fileType;
            reportParameter.BranchId = 0;
            reportParameter.ClassOfRiskId = 0;
            reportParameter.GroupBy = "AuthorisedRep";
            reportParameter.GroupPageBreak = false;
            reportParameter.InsurerId = 0;
            reportParameter.SalesTeamId = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.SelectAllRecords = false;
            reportParameter.SortBy = "Insurer";
            reportParameter.status = "All";
        }
    }
}
