﻿namespace INSIGHTReportTest.Report
{
    public class BatchClientNotesReport : ReportTestBase
    {
        public BatchClientNotesReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/BatchClientNotesReport";
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClientAutoComplete="(All)";
            reportParameter.ClientId=-1;
            reportParameter.FileType = fileType;
            reportParameter.FromDate="2019-04-01";
            reportParameter.GroupBy="AuthorisedRep";
            reportParameter.GroupPageBreak=false;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="Client";
            reportParameter.ToDate = "2020-04-01";
        }
    }
}
