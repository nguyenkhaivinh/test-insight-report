﻿
namespace INSIGHTReportTest.Report
{
    public class ClientAnalysisByAreaReport : ReportTestBase
    {

        public ClientAnalysisByAreaReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClientAnalysisByAreaReport";
            reportParameter.ExpiryDateFromDate = "2020-01-01";
            reportParameter.ExpiryDateToDate = "2020-03-01";
            reportParameter.FileType = fileType;
            reportParameter.SelectAllRecords = true;
            reportParameter.AreaId = "";

        }
    }
}
