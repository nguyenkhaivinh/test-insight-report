﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class SteadfastReport : ReportTestBase
    {
        public SteadfastReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/SteadfastReport";
            reportParameter.ClassId = "";
            reportParameter.FileType = fileType;
            reportParameter.FromDate = "2020-03-01";
            reportParameter.ToDate = "2020-03-15";
            reportParameter.GroupId = "";
            reportParameter.InsurerId = "";
            reportParameter.DateType = "Effective";
        }
       
    }
}
