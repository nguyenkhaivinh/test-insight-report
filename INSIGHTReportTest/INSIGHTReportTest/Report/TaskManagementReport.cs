﻿
namespace INSIGHTReportTest.Report
{
    public class TaskManagementReport : ReportTestBase
    {

        public TaskManagementReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/TaskManagementReport";
            reportParameter.FileType = fileType;

            reportParameter.ClaimQueue = "";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.ClientId = 0;
            reportParameter.ClientQueue = "";
            reportParameter.FromDate = "2020-02-01";
            reportParameter.GroupBy = "Client";
            reportParameter.IncludeClaimTask = true;
            reportParameter.IncludeClientTask = true;
            reportParameter.IncludePolicyTask = true;
            reportParameter.IncludeSystemRecords = false;
            reportParameter.Policy = "";
            reportParameter.QueuedTo = 0;
            reportParameter.SelectAllRecords = false;
            reportParameter.SortBy = "FollowupDate";
            reportParameter.Style = "Summary";
            reportParameter.TaskType = "AllTask";
            reportParameter.TaskTypeId = 0;
            reportParameter.ToDate = "2020-04-08";
        }
    }
}