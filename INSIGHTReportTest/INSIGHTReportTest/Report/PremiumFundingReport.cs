﻿
namespace INSIGHTReportTest.Report
{
    public class PremiumFundingReport : ReportTestBase
    {

        public PremiumFundingReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/PremiumFundingReport";
            reportParameter.FileType = fileType;

            reportParameter.FromDate="2020-04-01";
            reportParameter.GroupBy="Client";
            reportParameter.PaymentPlanStatus="";
            reportParameter.PremiumFunderId=0;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="ClientName";
            reportParameter.ToDate="2020-04-07";
        }
    }
}
