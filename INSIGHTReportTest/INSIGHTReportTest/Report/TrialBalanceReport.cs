﻿
namespace INSIGHTReportTest.Report
{
    public class TrialBalanceReport : ReportTestBase
    {
       
        public TrialBalanceReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/TrialBalanceReport";
            reportParameter.AsAtDate = "2020-04-06";
            reportParameter.Format = "Vertical";
            reportParameter.FileType = fileType;
            reportParameter.Style = "Summary";
        }
    }
}
