﻿
namespace INSIGHTReportTest.Report
{
    public class MailMergeClientsReport : ReportTestBase
    {

        public MailMergeClientsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/MailMergeClientsReport";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepAutoComplete="(All)";
            reportParameter.AuthorisedRepId	=0;
            reportParameter.BranchId="";
            reportParameter.ClassOfRiskId="";
            reportParameter.InsurerId="";
            reportParameter.PolicyExpiryFromDate="2020-01-01";
            reportParameter.PolicyExpiryToDate="2020-04-01";
            reportParameter.SalesTeamId="";
            reportParameter.SelectAllRecords=false;
            reportParameter.ServiceTeamId="";
        }
    }
}
