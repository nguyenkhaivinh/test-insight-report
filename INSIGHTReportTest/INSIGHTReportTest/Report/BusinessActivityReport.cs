﻿
namespace INSIGHTReportTest.Report
{
    public class BusinessActivityReport : ReportTestBase
    {

        public BusinessActivityReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/BusinessActivityReport";
            reportParameter.FromDate = "2020-03-27";
            reportParameter.ToDate = "2020-04-03";
            reportParameter.FileType = fileType;
            reportParameter.ReportType = "Invoiced";
            reportParameter.Style = "Summary";
        }
    }
}
