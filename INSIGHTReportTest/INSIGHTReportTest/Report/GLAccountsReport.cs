﻿
namespace INSIGHTReportTest.Report
{
    public class GLAccountsReport : ReportTestBase
    {
        public GLAccountsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/GLAccountsReport";
            reportParameter.FileType = fileType;
        }
    }
}
