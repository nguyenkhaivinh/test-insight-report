﻿namespace INSIGHTReportTest.Report
{
    public class SubLedgerAccountsReport : ReportTestBase
    {

        public SubLedgerAccountsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/SubLedgerAccountsReport";
            reportParameter.FileType = fileType;
            reportParameter.GLAccountNameId = 5;
        }
    }
}
