﻿
namespace INSIGHTReportTest.Report
{
    public class UnallocatedCashReturnsReport : ReportTestBase
    {

        public UnallocatedCashReturnsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/UnallocatedCashReturnsReport";
            reportParameter.FileType = fileType;
            reportParameter.AsAtDate = "2020-04-07";
        }
    }
}
