﻿
namespace INSIGHTReportTest.Report
{
    public class OverseasInsurersBordereauReport : ReportTestBase
    {
       
        public OverseasInsurersBordereauReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/OverseasInsurersBordereauReport";
            reportParameter.FromDate = "2020-01-06";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.BranchId = 0;
            reportParameter.ClassOfRiskId = 0;
            reportParameter.InsurerId = 0;
            reportParameter.PolicyTransactionTypeId = 0;
            reportParameter.SortBy = "Insurer";
            reportParameter.GroupBy = "None";
            reportParameter.Style = "Summary";
            reportParameter.DateOption = "InsurerPaidDate";
            reportParameter.PolicyType = "Paid";
        }
    }
}
