﻿
namespace INSIGHTReportTest.Report
{
    public class RenewalRetentionReport : ReportTestBase
    {

        public RenewalRetentionReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/RenewalRetentionReport";
            reportParameter.ExpiryDateFromDate = "2020-01-06";
            reportParameter.ExpiryDateToDate = "2020-04-06";
            reportParameter.AuthorisedRepAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.SalesTeamId = 0;
            reportParameter.GroupPageBreak = false;
            reportParameter.SelectAllRecords = true;
            reportParameter.PaymentPlan = "All";

            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClassOfRiskId=0;
            reportParameter.GroupBy="Branch";
            reportParameter.InsurerId=0;
            reportParameter.Mode="Policies";
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="Client";
            reportParameter.Style="Summary";
        }
    }
}
