﻿
namespace INSIGHTReportTest.Report
{
    public class ABNMissingReport : ReportTestBase
    {
        public ABNMissingReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/AbnMissingReport";

            reportParameter.SelectAllRecords = true;
            reportParameter.EntityName = "";
            reportParameter.EntityType = "";
            reportParameter.FileType = fileType;
        }

    }
}
