﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class InsurerMasterListReport : ReportTestBase
    {
        public InsurerMasterListReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InsurerMasterListReport";
            reportParameter.AsAtDate = "2020-02-24";
            reportParameter.ClassOfRiskId = -1;
            reportParameter.ExcludeNotCurrentRecords = true;
            reportParameter.FileType = fileType;
            reportParameter.InsurerId = 0;
            reportParameter.SelectAllRecords = true;
            reportParameter.SortBy = "InsurerName";
            reportParameter.Style = "Summary";
            reportParameter.Year = "FinancialYear";
        }
       
    }
}
