﻿using System;

namespace INSIGHTReportTest.Report
{
    public class ASICReturnsReport : ReportTestBase
    {

        public ASICReturnsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/AsicReturnsReport";
            reportParameter.AsAtDate = "2020-03-03";
            reportParameter.FromDate = "2020-03-01";
            reportParameter.ToDate = "2020-04-03";
            reportParameter.FileType = fileType;
            reportParameter.ReportType = "TrustAccountMovementsS26";
            reportParameter.Style = "Summary";

        }
    }
}
