﻿
namespace INSIGHTReportTest.Report
{
    public class EarningsReport : ReportTestBase
    {

        public EarningsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/EarningsReport";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepAutoComplete="(All)";
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClientAutoComplete="(All)";
            reportParameter.ClientId=0;
            reportParameter.FromDate="2020-02-01";
            reportParameter.GroupBy="Client";
            reportParameter.GroupPageBreak=false;
            reportParameter.PaymentPlan="All";
            reportParameter.ReportType="NetEarned";
            reportParameter.SalesTeamId=0;
            reportParameter.ServiceTeamId=0;
            reportParameter.ShowCreditCardFee=false;
            reportParameter.SortBy="None";
            reportParameter.ToDate = "2020-04-01";
        }
    }
}
