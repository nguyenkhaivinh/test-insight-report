﻿namespace INSIGHTReportTest.Report
{
    public class SunrisePolicyReport : ReportTestBase
    {

        public SunrisePolicyReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/SunrisePolicyReport";
            reportParameter.FromDate = "2020-01-06";
            reportParameter.ToDate = "2020-04-06";
            
            reportParameter.FileType = fileType;
            reportParameter.SalesTeamId = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.InsurerId = 0;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.BranchId = 0;
            reportParameter.SelectAllRecords = true;
            reportParameter.GroupBy = "Insurer";
            reportParameter.PaymentPlan = "All";
            reportParameter.PolicyType = "All";
            reportParameter.ProductCode = "(All)";
            reportParameter.SortBy = "Client";

        }
    }
}
