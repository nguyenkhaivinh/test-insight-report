﻿
namespace INSIGHTReportTest.Report
{
    public class CreditorsAgeingReport : ReportTestBase
    {

        public CreditorsAgeingReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/CreditorsAgeingReport";
            reportParameter.AsAt = "2020-03-27";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.AuthorisedRepAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.InsurerId = 0;
            reportParameter.ClientId = 0;

            reportParameter.AgeBand1=30;
            reportParameter.AgeBand2=60;
            reportParameter.AgeBand3=90;
            reportParameter.BranchId=0;
            reportParameter.CreditorType=0;
            reportParameter.GroupPageBreak=false;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="Client";
            reportParameter.Style="Summary";


        }
    }
}
