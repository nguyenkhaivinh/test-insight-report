﻿namespace INSIGHTReportTest.Report
{

    public class PolicyMasterListReport : ReportTestBase
    {

        public PolicyMasterListReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/PolicyMasterListReport";
            reportParameter.ExpiryFromDate = "2019-01-01";
            reportParameter.ExpiryToDate = "2020-12-31";
            reportParameter.ClientSelection = "AllClients";
            reportParameter.FileType = fileType;
            reportParameter.AreaId = -1;
            reportParameter.BranchId = -1;
            reportParameter.ClassOfRiskId = -1;
            reportParameter.InsurerId = 0;
            reportParameter.ClientId = 0;
            reportParameter.SalesTeamId = -1;
            reportParameter.ServiceTeamId = -1;
            reportParameter.AuthorisedRepId=0;
            reportParameter.IncludeCommission=false;
            reportParameter.IncludeImported=true;
            reportParameter.IncludeTakeUp=true;
            reportParameter.SelectAllRecords=false;
            reportParameter.PolicyTypes = "CurrentPolicies";
            reportParameter.SortBy = "Client";
            reportParameter.Style = "Summary";
            reportParameter.PaymentPlan = "All";
            reportParameter.PolicyNumber = "";

        }
    }
}
