﻿
namespace INSIGHTReportTest.Report
{
    public class GLTransactionReport : ReportTestBase
    {

        public GLTransactionReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/GLTransactionReport";
            reportParameter.FromDate = "2020-03-27";
            reportParameter.ToDate = "2020-04-03";
            reportParameter.FileType = fileType;
            reportParameter.EntityAutoComplete = "(All)";
            reportParameter.EntityId = 0;
            reportParameter.GroupPageBreak = false;
            reportParameter.PolicyTransactionTypeId="";
            reportParameter.ReportType="GLTransaction";
        }
    }
}
