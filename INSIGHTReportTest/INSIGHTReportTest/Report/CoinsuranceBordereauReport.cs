﻿
namespace INSIGHTReportTest.Report
{    public class CoinsuranceBordereauReport : ReportTestBase
    {

        public CoinsuranceBordereauReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/CoinsuranceBordereauReport";
            reportParameter.FromDate = "2018-01-01";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.FileType = fileType;
            reportParameter.SelectAllRecords = true;
            reportParameter.SortBy = "ClassOfBusiness";
            reportParameter.GroupBy = "ClassOfBusiness";
        }
    }
}
