﻿
namespace INSIGHTReportTest.Report
{
    public class BusinessWrittenReport : ReportTestBase
    {
        public BusinessWrittenReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/BusinessWrittenReport";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = "";
            reportParameter.BranchId="";
            reportParameter.ClassOfRiskId="";
            reportParameter.DateOption="DatePosted";
            reportParameter.ExcludeCreditCardFees=false;
            reportParameter.FromDate="2020-04-01";
            reportParameter.GroupBy="None";
            reportParameter.IncludeBasePremium=false;
            reportParameter.IncludeFullyPaidTransactionsOnly=false;
            reportParameter.IncludeImported=false;
            reportParameter.InsurerId="";
            reportParameter.InterfaceType="";
            reportParameter.PaymentPlan="All";
            reportParameter.PolicyTransactionTypeId="";
            reportParameter.SalesTeamId="";
            reportParameter.ServiceTeamId="";
            reportParameter.SortBy="AuthorisedRep";
            reportParameter.SourceOfBusinessId="";
            reportParameter.SubTotalOnSortBy=false;
            reportParameter.ToDate="2020-04-01";
        }
    }
}
