﻿
namespace INSIGHTReportTest.Report
{
    public class ReceiptsAnalysisReport : ReportTestBase
    {

        public ReceiptsAnalysisReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ReceiptsAnalysisReport";
            reportParameter.FileType = fileType;

            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClassOfRiskId=0;
            reportParameter.ExcludeCancellationTransactions=false;
            reportParameter.ExcludeLapseTransactions=false;
            reportParameter.FromDate="2020-04-01";
            reportParameter.GroupBy="None";
            reportParameter.IncludeGST=false;
            reportParameter.InsurerId=0;
            reportParameter.PaymentPlan="All";
            reportParameter.PolicyTransactionTypeId=0;
            reportParameter.SalesTeamId=0;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="ClientName";
            reportParameter.SubTotalOnClientName=false;
            reportParameter.ToDate="2020-04-07";
        }
    }
}
