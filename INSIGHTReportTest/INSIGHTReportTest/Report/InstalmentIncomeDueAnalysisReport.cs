﻿

namespace INSIGHTReportTest.Report
{

    public class InstalmentIncomeDueAnalysisReport : ReportTestBase
    {

        public InstalmentIncomeDueAnalysisReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InstalmentIncomeDueAnalysisReport";
            reportParameter.AsAtDate = "2020-04-06";
            reportParameter.AuthorisedRepAutoComplete = "(All)";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.BranchId = 0;
            reportParameter.ClientId = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.SalesTeamId = 0;
            reportParameter.SelectAllRecords = false;
            reportParameter.SortBy = "Client";
            reportParameter.GroupBy = "Branch";
            reportParameter.Style = "Summary";
            reportParameter.InsurerId = "";
            reportParameter.InterfaceType = "";

        }
    }
}
