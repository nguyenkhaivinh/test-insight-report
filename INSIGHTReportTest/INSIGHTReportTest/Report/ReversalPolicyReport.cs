﻿namespace INSIGHTReportTest.Report
{
    public class ReversalPolicyReport : ReportTestBase
    {

        public ReversalPolicyReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ReversalPolicyReport";
            reportParameter.FromDate = "2020-01-06";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.ReportType = "ReversalReport";
            reportParameter.FileType = fileType;
            reportParameter.SalesTeamId = 0;
            reportParameter.InsurerId = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.SortBy = "None";

        }
    }
}
