﻿
namespace INSIGHTReportTest.Report
{
    public class CashReceiptsReport : ReportTestBase
    {

        public CashReceiptsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/CashReceiptsReport";
            reportParameter.FromDate = "2020-01-06";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.ClientId = 0;
            reportParameter.EntityType = "Client";
            reportParameter.InsurerAutoComplete = "(All)";
            reportParameter.InsurerId = 0;

            reportParameter.NetAuthorisedRepAutoComplete="(All)";
            reportParameter.NetAuthorisedRepId=0;
            reportParameter.ReportType="CashReceipts";


        }
    }
}
