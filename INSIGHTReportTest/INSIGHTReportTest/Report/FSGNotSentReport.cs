﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class FSGNotSentReport : ReportTestBase
    {
        public FSGNotSentReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/FSGReport";
            reportParameter.BranchId = 0;
            reportParameter.FileType = fileType;
            reportParameter.FromDate = "2019-05-01";
            reportParameter.ToDate = "2020-02-24";
            reportParameter.IncludeTakeUp = true;
            reportParameter.ReportType = "AllClients";
        }
        
    }
}
