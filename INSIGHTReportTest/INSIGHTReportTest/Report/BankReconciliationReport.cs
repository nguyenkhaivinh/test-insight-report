﻿namespace INSIGHTReportTest.Report
{
    public class BankReconciliationReport : ReportTestBase
    {
        public BankReconciliationReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/BankReconciliationReport";
            reportParameter.ReconciliationDate = "2020-04-03";
            reportParameter.ReportType = "Reconciled";
            reportParameter.FileType = fileType;
            reportParameter.Style = "Detail";
        }
    }
}
