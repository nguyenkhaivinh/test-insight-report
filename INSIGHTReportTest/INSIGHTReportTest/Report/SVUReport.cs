﻿
namespace INSIGHTReportTest.Report
{
    public class SVUReport : ReportTestBase
    {

        public SVUReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/SVUReport";
            reportParameter.FromDate = "2020-01-06";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.GroupBy = "Insurer";
            reportParameter.PaymentPlan = "All";
            reportParameter.SortBy = "Insurer";
            reportParameter.FileType = fileType;
            reportParameter.SalesTeamId = 0;
            reportParameter.BranchId = 0;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.ClassOfRiskId = 0;
            reportParameter.ClientId = 0;
            reportParameter.InsurerId = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.TransactionType = 0;
            reportParameter.SelectAllRecords = true;

        }
    }
}