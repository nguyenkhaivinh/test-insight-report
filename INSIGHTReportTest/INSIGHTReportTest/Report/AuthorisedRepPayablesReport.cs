﻿using Newtonsoft.Json.Linq;
using System;
using System.Dynamic;
using System.IO;

namespace INSIGHTReportTest.Report
{
    public class AuthorisedRepPayablesReport : ReportTestBase
    {
        public AuthorisedRepPayablesReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ARPaymentReport";
            reportParameter.AuthorisedRepId = IsAUMode ? 1004 : 4653; // NZ: Alfonso Ellison
            reportParameter.FileType = fileType;
            reportParameter.SelectionValues = "current";
            reportParameter.SpId = GetSpid(@"/Services/Payments/ListAuthorisedRepsForPayment");
            
            //reportParameter.PayableList = new dynamic[3];
            

            //reportParameter.PayableList[0] = new ExpandoObject();
            //reportParameter.PayableList[0].AuthorisedRepId = 3682;
            //reportParameter.PayableList[0].AuthorisedRepName = "Auto_AR__1561_1673_38";
            //reportParameter.PayableList[0].AuthorisedRepTradingAs = "TRA_AR0001";
            //reportParameter.PayableList[0].AuthorisedRepRef = "Ref_AR0001";
            //reportParameter.PayableList[0].NetPayable = 36.3;
            //reportParameter.PayableList[0].AcceptPartPayment = false;
            //reportParameter.PayableList[0].AuthorisedRepEmailAddress = "lins@sfg.local";
            //reportParameter.PayableList[0].PolicyTransactionCount = 1;
            //reportParameter.PayableList[0].ABAFileGen = false;

            //reportParameter.PayableList[1] = new ExpandoObject();
            //reportParameter.PayableList[1].AuthorisedRepId = 3716;
            //reportParameter.PayableList[1].AuthorisedRepName = "Auto_AR__1561_3255_81";
            //reportParameter.PayableList[1].AuthorisedRepTradingAs = "TRA_AR0001";
            //reportParameter.PayableList[1].AuthorisedRepRef = "Ref_AR0001";
            //reportParameter.PayableList[1].NetPayable = -40.7;
            //reportParameter.PayableList[1].AcceptPartPayment = false;
            //reportParameter.PayableList[1].AuthorisedRepEmailAddress = "lins@sfg.local";
            //reportParameter.PayableList[1].PolicyTransactionCount = 1;
            //reportParameter.PayableList[1].ABAFileGen = false;

            //reportParameter.PayableList[2] = new ExpandoObject();
            //reportParameter.PayableList[2].AuthorisedRepId = 3682;
            //reportParameter.PayableList[2].AuthorisedRepName = "Auto_AR__1561_3256_33";
            //reportParameter.PayableList[2].AuthorisedRepTradingAs = "TRA_AR0002";
            //reportParameter.PayableList[2].AuthorisedRepRef = "Ref_AR0002";
            //reportParameter.PayableList[2].NetPayable = -51.72;
            //reportParameter.PayableList[2].AcceptPartPayment = false;
            //reportParameter.PayableList[2].AuthorisedRepEmailAddress = "auto_AR@insighttesting.com.au";
            //reportParameter.PayableList[2].PolicyTransactionCount = 1;
            //reportParameter.PayableList[2].ABAFileGen = true;

        }
    }
}
