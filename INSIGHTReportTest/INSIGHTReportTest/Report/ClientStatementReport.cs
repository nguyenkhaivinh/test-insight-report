﻿
using System.Dynamic;
namespace INSIGHTReportTest.Report
{

    public class ClientStatementReport : ReportTestBase
    {

        public ClientStatementReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClientStatementReport";
            
            reportParameter.AgeBand1=7;
            reportParameter.AgeBand2=60;
            reportParameter.AgeBand3=90;
            reportParameter.AsAtDate= "2019-12-31";
            reportParameter.AuthorisedRepAutoComplete="(All)";
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClientAutoComplete="";
            reportParameter.ClientId="";
            
            reportParameter.DaysOutstanding=0;
            reportParameter.DisplayPolicyDescription=false;
            reportParameter.DownloadReport=false;
            reportParameter.EntIds = IsAUMode
                ? new int[] { 20044, 5920, 2954, 20314, 20204, 18791, 14089, 20443, 6140, 9665, 3943, 3907, 2231, 8564, 21566, 5684, 8092, 8033, 17087, 5910, 14220, 19092, 17135, 19267, 15504, 3477, 4144, 20324, 3848, 6347, 20367, 10301, 17269, 7479, 3643, 15463, 3137, 13466, 15347, 18989, 17052, 7184, 5748, 20351, 10673, 15573, 17749, 19123, 15191, 3266, 9385, 5846, 5714, 12537, 21383, 17258, 14282, 3728, 4076, 15100, 19102, 1551, 2893, 19777, 2203, 7960, 17025, 4061, 14734, 13933, 20148, 14192, 19067, 3787, 1041, 20171, 12894, 15624, 12785, 12890, 8965, 13290, 20280, 19978, 20260, 3140, 3285, 14387, 6169, 15356, 14929, 3880, 17259, 12064, 20436, 8896, 12702, 14618, 8046, 8950, 3452, 5923, 21463, 20103, 17074, 1180, 15640, 3216, 14079, 13968, 3670, 20545, 15556, 19027, 21529, 20418, 3889, 2493, 2267, 10436, 3487, 21270, 13583, 20480, 2945, 6776, 3225, 13035, 16709, 11510, 2415, 17317, 15670, 5479, 12523, 13194, 14373, 3774, 5884, 3548, 5640, 6761, 3805, 10103, 20503, 20102, 5310, 10065, 16812, 15192, 20239, 19140, 20267, 3289, 3628, 20561, 11662, 19007, 1427, 5869, 16821, 15413, 20061, 13225, 20124, 20338, 20085, 18455, 12699, 16930, 8043, 2606, 10599, 5350, 5138, 6355, 16523, 5514, 3554, 14099, 14207, 14132, 2963, 15399, 5355, 18155, 19368, 20301, 21563, 17572, 18702, 10632, 15755, 18017, 8078, 8343, 16936, 13454, 14183, 14685, 3531, 21346, 20321, 2721, 19168, 12478, 3341, 19059, 16803, 6004, 18102, 5369, 2664, 21486, 4964, 20169, 14060, 14386, 18995, 21488, 14509, 17180, 16530, 5599, 21441, 2426, 17163, 2447, 14892, 6133, 19174, 13219, 20384, 2261, 20452, 13553, 14163, 20291, 20327, 3685, 21542, 20445, 12034, 14339, 19002, 21119, 3744, 3282, 18609, 18549, 8570, 14272, 19038, 8079, 20345, 20471, 6356, 7181, 13914, 19398, 13228, 21369, 2478, 3843, 17130, 18509, 5707, 17105, 5824, 18706, 21082, 3419, 12451, 5422, 1213, 8507, 9143, 16845, 12323, 7551, 3731, 6857, 17356, 3465, 12756, 20413, 17136, 5412, 18082, 16967, 3279, 20285, 17184, 14667, 13140, 20400, 3998, 16900, 5829, 16850, 2927, 17001, 6554, 15750, 6269, 8522, 19060, 13591, 3019, 6143, 15414, 16034, 4522, 19162, 15780, 10336, 10994, 12618, 10461, 13237, 2827, 5128, 20430, 13990, 11017, 13635, 3519, 20296, 11099, 20214, 14061, 20377, 6132, 10604, 20463, 3734, 6317, 20892, 18164, 2304, 15348, 17189, 18530, 15564, 15484, 1797, 18327, 15490, 17214, 12317, 21395, 18985, 13954, 15492, 17195, 20263, 5235, 18962, 5608, 3938, 6296, 5323, 21548, 16160, 3584, 21430, 8948, 17093, 15690, 9410 }
                : new int[] { 1555, 1068, 3167, 1092, 3223, 4421, 1749, 4536, 834, 357, 2881, 2769, 310, 815, 1426, 1630, 1561, 178, 749, 595, 1035, 1481, 612, 4451, 3127, 4504, 4439, 1452, 2726, 1154, 3029, 3161, 899, 4440, 2658, 1109, 1411, 460, 3201, 917, 3041, 1889, 1858, 2280, 189, 4475, 1282, 827, 2010, 3151, 2311, 1276, 2695, 315, 894, 2803, 2863, 478, 1882, 2059, 2425, 662, 2784, 1072, 2780, 3072, 4544, 2788, 1754, 3187, 1782, 2835, 2840, 2846, 522, 3059, 4458, 1252, 1320, 1556, 3322, 3009, 2121, 1031, 2229, 2681, 767, 4367, 2521, 4335, 1146, 1632, 1545, 1160, 2808, 1067, 1458, 1718, 1190, 4387, 351, 2269, 1293, 2234, 2553, 1347, 3222, 1101, 2678, 1790, 216, 1367, 1107, 2195, 1644, 1831, 3323, 1658, 1936, 3020, 2715, 671, 2170, 3216, 549, 2455, 2038, 2191, 3214, 2335, 2309, 1878, 1448, 527, 953, 1376, 902, 4503, 1535, 1768, 840, 1686, 4545, 4424, 3154, 4480, 1713, 2652, 2810, 326, 2153, 1992, 1460, 443, 1917, 2274, 758, 552, 1344, 418, 791, 774, 3007, 856, 440, 2256, 1220, 3195, 932, 4510, 723, 396, 708, 489, 2420, 778, 4474, 687, 1464, 2576, 366, 833, 874, 1191, 2638, 1255, 1666, 2509, 1897, 2356, 3218, 3202, 828, 2296, 2442, 998, 3224, 1112, 4527, 2328, 1265, 548, 2884, 2068, 4511, 1440, 634, 267, 824, 1657, 959, 1207, 1994, 4390, 1691, 2934, 4499, 2557, 3168, 3102, 4534, 2490, 4411, 4374, 1801, 1375, 2862, 2853, 3063, 1364, 1133, 2303, 2839, 1637, 3104, 476, 2049, 2424, 3060, 3312, 540, 3217, 1451, 2824, 2849, 2313, 2177, 647, 2192, 1562, 2092, 1442, 2249, 1770, 879, 398, 1719, 1493, 1675, 2300, 1597, 1939, 4417, 2477, 3156, 2370, 620, 772, 1372, 2544, 895, 1785, 1143, 1851, 2642, 1965, 1856, 930, 1040, 299, 762, 2161, 2468, 1279, 1885, 1706, 1778, 2699, 4516, 2142, 727, 2981, 3189, 2815, 3162, 1930, 2276, 564, 3194, 2984, 4466, 2819, 1414, 2870, 282, 2217, 1867, 2000, 714, 544, 4364, 653, 2381, 508, 878, 2283, 2661, 2549, 2279, 378, 1263, 2242, 1019, 2476, 3147, 2836, 2814, 1809, 1083, 2542, 722, 1082, 2927, 4415, 2124, 2749, 2039, 1750, 2635, 2166, 1654, 858, 1971, 1487, 1130, 2134, 2851, 1322, 2417, 4460, 1244, 4539, 3209, 1852, 1497, 2402, 1131, 2084, 1508, 2753, 590, 2262, 453, 1868, 3236, 1246, 3191, 718, 2620, 1973, 1337, 975, 2141, 3164, 2176, 1338, 2130, 3244, 2119, 721, 3205, 2457, 1860, 311, 1641, 633, 3226 };
            reportParameter.FromDate="2020-03-01";
            reportParameter.Id=0;
            reportParameter.IncludePaidInvoices=false;
            reportParameter.ReportBy="BatchClient";
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.SendEmail=false;
            reportParameter.ServiceTeamId=0;
            reportParameter.ShowSubTotals=false;
            reportParameter.SortBy="AuthorisedRep";
            reportParameter.StatementText="";
            reportParameter.Style="Detail";
            reportParameter.ToDate="2020-04-01";

            reportParameter.ClientSelection=new ExpandoObject();
            //reportParameter.ClientSelection.PageCount = 0;
            //reportParameter.ClientSelection.PageNumber = 0;
            //reportParameter.ClientSelection.Results = new dynamic[2];
            //reportParameter.ClientSelection.Results[0] = new ExpandoObject();
            //reportParameter.ClientSelection.Results[0].Balance=62790.4f;
            //reportParameter.ClientSelection.Results[0].ClientId=3980;
            //reportParameter.ClientSelection.Results[0].ClientName="AUTO CLIENT FSG 1";
            //reportParameter.ClientSelection.Results[0].Current=6249.4f;
            //reportParameter.ClientSelection.Results[0].GreaterThanDay1=31708.4f;
            //reportParameter.ClientSelection.Results[0].GreaterThanDay2=11204.92f;
            //reportParameter.ClientSelection.Results[0].GreaterThanDay3=13627.68f;
            //reportParameter.ClientSelection.Results[0].InvoiceAmount=62790.4f;
            //reportParameter.ClientSelection.Results[0].IsCoClient=false;
            //reportParameter.ClientSelection.Results[0].IsSelected=true;
            //reportParameter.ClientSelection.Results[0].LastPaymentDate = "";
            //reportParameter.ClientSelection.Results[0].NotDue=0;
            //reportParameter.ClientSelection.Results[0].PaymentAmount=0;

            //reportParameter.ClientSelection.Results[1] = new ExpandoObject();
            //reportParameter.ClientSelection.Results[1].Balance = 1531.53f;
            //reportParameter.ClientSelection.Results[1].ClientId = 3683;
            //reportParameter.ClientSelection.Results[1].ClientName = "Auto_Client__1561_1673_85";
            //reportParameter.ClientSelection.Results[1].Current = 0;
            //reportParameter.ClientSelection.Results[1].GreaterThanDay1 = 0;
            //reportParameter.ClientSelection.Results[1].GreaterThanDay2 = 0;
            //reportParameter.ClientSelection.Results[1].GreaterThanDay3 = 0;
            //reportParameter.ClientSelection.Results[1].InvoiceAmount = 4166.53f;
            //reportParameter.ClientSelection.Results[1].IsCoClient = false;
            //reportParameter.ClientSelection.Results[1].IsSelected = true;
            //reportParameter.ClientSelection.Results[1].LastPaymentDate = "2019-07-01";
            //reportParameter.ClientSelection.Results[1].NotDue = 0;
            //reportParameter.ClientSelection.Results[1].PaymentAmount = 10000;

        }
    }
}
