﻿namespace INSIGHTReportTest.Report
{
    public class DiaryReminderReport : ReportTestBase
    {

        public DiaryReminderReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/DiaryReminderReport";
            reportParameter.FileType = fileType;

            reportParameter.AuthorisedRepId = 0;
            reportParameter.AreaId=0;
            reportParameter.AuthorisedRepAutoComplete="(All)";
            reportParameter.BranchId=0;
            reportParameter.ClassOfRiskId=0;
            reportParameter.ClientId=0;
            reportParameter.ClientIdAutoComplete="(All)";
            reportParameter.DateOption="All";
            reportParameter.ExcludeCancelledPolicies=false;
            reportParameter.ExcludeLapsedPolicies=false;
            reportParameter.ExpireCoverNote=true;
            reportParameter.ExpireCoverNoteDate="2020-04-08";
            reportParameter.FromDate="2020-03-01";
            reportParameter.IncludePoliciesClosedButNotPaid=true;
            reportParameter.IncludePoliciesNotClosed=true;
            reportParameter.InsurerId=0;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=false;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="Client";
            reportParameter.Style="Summary";
            reportParameter.ToDate="2020-04-08";
        }
    }
}
