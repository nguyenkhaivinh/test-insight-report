﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class InsurerStatementReport : ReportTestBase
    {
        public InsurerStatementReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InsurerStatementReport";
            reportParameter.AsAtDate = "2020-02-24";
            reportParameter.FileType = fileType;
            reportParameter.IncludeComments = false;
            reportParameter.IncludePaidInvoices = false;
            reportParameter.InsurerAutoComplete = IsAUMode ? "AIG Direct IBUAT" : "AIG Insurance NZ Ltd";
            reportParameter.InsurerId = IsAUMode ? 4208 : 114;
            reportParameter.ShowSubtotals = false;
            reportParameter.StatementText = "";
            reportParameter.UseBranchDetails = false;
        }
        
    }
}
