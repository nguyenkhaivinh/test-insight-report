﻿
namespace INSIGHTReportTest.Report
{

    public class PortfolioAnalysisReport : ReportTestBase
    {

        public PortfolioAnalysisReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/PortfolioAnalysisReport";

            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.BranchId=0;
            reportParameter.ClassOfRiskId=0;
            reportParameter.ExpiryFromDate="2020-02-01";
            reportParameter.ExpiryToDate="2020-04-01";
            reportParameter.IncludeImported=true;
            reportParameter.IncludeTakeUp=true;
            reportParameter.IncomeOrder=true;
            reportParameter.InsurerId=0;
            reportParameter.NetIncome=true;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="AuthorisedRep";
        }
    }
}
