﻿using Newtonsoft.Json.Linq;
using System;

namespace INSIGHTReportTest.Report
{
    public class APRAForm701Report : ReportTestBase
    {
        public APRAForm701Report(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/dofiReport";
            reportParameter.FromDate = "2019-02-24";
            reportParameter.ToDate = "2020-02-24";
            reportParameter.FileType = fileType;
        }
       
    }
}
