﻿
namespace INSIGHTReportTest.Report
{
    public class IbaReconciliationReport : ReportTestBase
    {

        public IbaReconciliationReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/IbaReconciliationReport";
            reportParameter.FromDate = "2020-03-27";
            reportParameter.ToDate = "2020-04-04";
            reportParameter.FileType = fileType;
        }
    }
}
