﻿
using System.Dynamic;

namespace INSIGHTReportTest.Report
{
    public class IndividualClientNotesReport : ReportTestBase
    {

        public IndividualClientNotesReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/IndividualClientNotesReport";
            reportParameter.FromDate = "2010-11-09";
            reportParameter.ToDate = "2020-06-12";
            reportParameter.DateSelection = "All";
            reportParameter.FileType = fileType;
            reportParameter.ClientId = IsAUMode ? 3429 : 1667;
            reportParameter.NoteIds = IsAUMode
                ? new int[] { 58879, 2465, 20653, 33293, 74040, 74377, 79993, 81006, 81616, 81854 }
                : new int[] { 149 };

        }
    }
}
