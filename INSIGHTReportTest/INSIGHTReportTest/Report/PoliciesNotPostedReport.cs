﻿using System;

namespace INSIGHTReportTest.Report
{
    public class PoliciesNotPostedReport : ReportTestBase
    {

        public PoliciesNotPostedReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/PoliciesNotPostedReport";
            reportParameter.FromDate = "2020-01-06";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.TransactionType = "All";
            reportParameter.FileType = fileType;
            reportParameter.SalesTeamId = -1;
            reportParameter.IncludeCoverNotes = true;
            reportParameter.IncludeLapses = true;
            reportParameter.IncludeQuotations = true;

        }
    }
}
