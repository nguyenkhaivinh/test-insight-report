﻿
namespace INSIGHTReportTest.Report
{
    public class GeneralLedgerReport : ReportTestBase
    {

        public GeneralLedgerReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/GeneralLedgerReport";
            reportParameter.FromDate = "2020-03-27";
            reportParameter.ToDate = "2020-04-03";
            reportParameter.FileType = fileType;
            reportParameter.Style = "Summary";
            reportParameter.GLAccountId = 0;
            reportParameter.SubLedgerId = 0;
        }
    }
}
