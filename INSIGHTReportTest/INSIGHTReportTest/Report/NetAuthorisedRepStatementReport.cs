﻿
namespace INSIGHTReportTest.Report
{
    public class NetAuthorisedRepStatementReport : ReportTestBase
    {

        public NetAuthorisedRepStatementReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ARStatementReport";
            reportParameter.AsAtDate = "2020-01-01";
            reportParameter.ClientId = 0;
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.AuthorisedRepId = "";
            reportParameter.FileType = fileType;
            reportParameter.IncludePaidInvoices = false;
            reportParameter.StatementText = "";
            reportParameter.Style = "Summary";
        }
    }
}
