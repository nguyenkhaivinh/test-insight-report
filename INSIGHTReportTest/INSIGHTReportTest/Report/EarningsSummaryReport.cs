﻿
namespace INSIGHTReportTest.Report
{
    public class EarningsSummaryReport : ReportTestBase
    {

        public EarningsSummaryReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/EarningsSummaryReport";
            reportParameter.FromDate = "2020-02-01";
            reportParameter.ToDate = "2020-04-01";
            reportParameter.PaymentPlan = "All";
            reportParameter.FileType = fileType;
            reportParameter.GroupBy = "Branch";
            reportParameter.Style = "Summary";
        }
    }
}
