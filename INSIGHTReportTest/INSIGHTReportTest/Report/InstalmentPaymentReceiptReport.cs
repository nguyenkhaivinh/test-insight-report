﻿
namespace INSIGHTReportTest.Report
{
    public class InstalmentPaymentReceiptReport : ReportTestBase
    {

        public InstalmentPaymentReceiptReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InstalmentPaymentReceiptReport";
            reportParameter.FileType = fileType;

            reportParameter.BranchId=0;
            reportParameter.ClientAutoComplete="(All)";
            reportParameter.ClientId=0;
            reportParameter.DateOption="DateReceipted";
            reportParameter.FromDate="2015-09-01";
            reportParameter.GroupBy="PaymentReference";
            reportParameter.InsurerCode="";
            reportParameter.InsurerIndex=0;
            reportParameter.PaymentReference="(All)";
            reportParameter.PolicyNumber="(All)";
            reportParameter.ProcessedStatus=-999;
            reportParameter.SelectAllRecords=true;
            reportParameter.SortBy="ProcessedOrder";
            reportParameter.ToDate="2020-04-07";
        }
    }
}
