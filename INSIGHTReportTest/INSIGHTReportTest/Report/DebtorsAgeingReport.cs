﻿
namespace INSIGHTReportTest.Report
{
    public class DebtorsAgeingReport : ReportTestBase
    {

        public DebtorsAgeingReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/DebtorsAgeingReport";
            reportParameter.AsAt = "2020-01-06";
            reportParameter.ClientType = "masterClientAndStandAloneOnly";
            reportParameter.AuthorisedRepAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.ClientId = 0;
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.SortBy = "Client";
            reportParameter.InsurerId = 0;

            reportParameter.GroupBy = "AuthorisedRep";
            reportParameter.DaysBetween = 0;
            reportParameter.ContentType = "Premium";
            reportParameter.Style = "Summary";

            reportParameter.AgeBand1=30;
            reportParameter.AgeBand2=60;
            reportParameter.AgeBand3=90;
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.GroupPageBreak=false;
            reportParameter.PaymentPlanStatusId="";
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;

        }
    }
}
