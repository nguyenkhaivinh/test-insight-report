﻿
namespace INSIGHTReportTest.Report
{
    public class ClaimReport : ReportTestBase
    {

        public ClaimReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClaimsReport";
            
            reportParameter.AuthRepId=0;
            reportParameter.BranchId=0;
            reportParameter.CauseId=0;
            reportParameter.ClaimStatusId=0;
            reportParameter.ClassOfRiskId=0;
            reportParameter.ClientAutoComplete="(All)";
            reportParameter.ClientId=0;
            reportParameter.ClosedStatus="All";
            reportParameter.DateType="Reported";
            reportParameter.FileType= fileType;
            reportParameter.FromDate="2020-03-01";
            reportParameter.GroupBy="Client";
            reportParameter.InsurerId=0;
            reportParameter.ReportType=8;
            reportParameter.ResultId=0;
            reportParameter.SalesTeamId=0;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy = "Client";
            reportParameter.SubjectId=0;
            reportParameter.ToDate="2020-03-31";
        }
    }
}
