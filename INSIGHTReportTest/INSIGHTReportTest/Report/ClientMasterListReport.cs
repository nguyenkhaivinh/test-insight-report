﻿
namespace INSIGHTReportTest.Report
{
    public class ClientMasterListReport : ReportTestBase
    {
        public ClientMasterListReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClientMasterListReport";
            reportParameter.AreaId=0;
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.CoClientSelection=0;;
            reportParameter.EntityStatusId=0;
            reportParameter.EntityTypeId=8;
            reportParameter.ExpiryDateFromDate="2019-04-01";
            reportParameter.ExpiryDateToDate="2020-04-30";
            reportParameter.FileType = fileType;
            reportParameter.GroupBy="Client";
            reportParameter.IncludeTakeUp=true;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=false;
            reportParameter.ServiceTeamId=0;
            reportParameter.SourceOfBusinessId=0;
            reportParameter.Style="Summary";
            reportParameter.UseDateRange = true;
        }
    }
}
