﻿namespace INSIGHTReportTest.Report
{
    public class EventPublicationResyncReport : ReportTestBase
    {

        public EventPublicationResyncReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/EventPublicationResyncReport";
            reportParameter.FileType = fileType;

            reportParameter.AuthorisedRepAutoComplete="";
            reportParameter.AuthorisedRepId="";
            reportParameter.ClientAutoComplete="AUTO_TEST_CLIENT_1";
            reportParameter.ClientId=118;
            reportParameter.EntityType="Client";
            reportParameter.InsurerAutoComplete="";
            reportParameter.InsurerId="";
            reportParameter.Scope="Single";
        }
    }
}
