﻿
namespace INSIGHTReportTest.Report
{
    public class InsurerPayablesReport : ReportTestBase
    {
       
        public InsurerPayablesReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InsurerPaymentReport";
            
            reportParameter.Date = "2020-06-12";
            reportParameter.DateSelection = "All";
            reportParameter.FileType = fileType;
            reportParameter.FullyPaidPoliciesOnly = false;
            reportParameter.InsurerId = IsAUMode ? 19597 : 118; // "Ando Insurance Group Ltd"
            reportParameter.SpId = GetSpid(@"/services/Payments/ListInsurersForPayment");
        }
    }
}
