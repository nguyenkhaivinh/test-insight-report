﻿
namespace INSIGHTReportTest.Report
{
    public class DrawingsReport : ReportTestBase
    {

        public DrawingsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/DrawingsReport";
            reportParameter.FromDate = "2018-01-01";
            reportParameter.ToDate = "2020-04-03";
            reportParameter.FileType = fileType;
            reportParameter.DrawingsPaybatchId = "";
            reportParameter.GroupBy = "None";
            reportParameter.SortBy = "Client";
            reportParameter.Style = "Summary";
        }
    }
}
