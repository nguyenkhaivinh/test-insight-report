﻿
using INSIGHTReportTest.Util;

namespace INSIGHTReportTest.Report
{
    public class PaymentReversalReport : ReportTestBase
    {

        public PaymentReversalReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/PaymentReversalReport";
            reportParameter.FromDate = Config.Country == "AU" ? "2020-03-27" : "2000-09-01";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.ClientAutoComplete = "(All)";
            reportParameter.FileType = fileType;
            reportParameter.AuthorisedRepId = 0;
            reportParameter.InsurerId = 0;
            reportParameter.ClientId = 0;
            reportParameter.SortBy = "PaymentNumber";
            reportParameter.GroupBy = "None";
            reportParameter.Payee = "Insurer";

        }
    }
}
