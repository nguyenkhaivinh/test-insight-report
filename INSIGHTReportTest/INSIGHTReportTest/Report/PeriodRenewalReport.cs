﻿namespace INSIGHTReportTest.Report
{
    public class PeriodRenewalReport : ReportTestBase
    {

        public PeriodRenewalReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/PeriodRenewalReport";
            reportParameter.ExpiryDateFromDate = "2020-01-06";
            reportParameter.ExpiryDateToDate = "2020-04-06";
            reportParameter.AsAtDate = "2020-04-07";
            reportParameter.FileType = fileType;
            reportParameter.AreaId=0;

            reportParameter.AuthorisedRepAutoComplete="(All)";
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClassOfRiskId=0;
            reportParameter.DateOption="ExpiryDatePeriod";

            reportParameter.GroupBy="AuthorisedRep";
            reportParameter.GroupPageBreak=false;
            reportParameter.InsurerId=0;
            reportParameter.PaymentPlan="All";
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="RenewalDate";
            reportParameter.SplitInvoiceSelection=0;
            reportParameter.Style="Summary";
        }
    }
}
