﻿namespace INSIGHTReportTest.Report
{
    public class GLJournalReport : ReportTestBase
    {

        public GLJournalReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/GLJournalReport";
            reportParameter.FromDate = "2020-03-27";
            reportParameter.FromDate = "2020-04-03";
            reportParameter.ClientAutoComplete = "All";
            reportParameter.FileType = fileType;
            reportParameter.GroupBy = "None";
            reportParameter.ReportType = "GLJournal";
            reportParameter.ClientId = 0;
            reportParameter.InsurerId = 0;
        }
    }
}
