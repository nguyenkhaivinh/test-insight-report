﻿
namespace INSIGHTReportTest.Report
{
    public class ClientAnalysisByIndustryReport : ReportTestBase
    {

        public ClientAnalysisByIndustryReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClientAnalysisByIndustryReport";
            reportParameter.ExpiryDateFromDate = "2020-02-01";
            reportParameter.ExpiryDateToDate = "2020-04-01";
            reportParameter.Style = "Summary";
            reportParameter.FileType = fileType;
            reportParameter.SelectAllRecords = true;
            reportParameter.IndustryId = "";
            
        }
    }
}
