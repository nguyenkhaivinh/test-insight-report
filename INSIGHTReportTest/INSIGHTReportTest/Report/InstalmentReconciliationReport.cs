﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.Report
{
    public class InstalmentReconciliationReport : ReportTestBase
    {

        public InstalmentReconciliationReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/InstalmentReconciliationReport";
            reportParameter.FromDate = "2016-04-06";
            reportParameter.ToDate = "2020-04-06";
            reportParameter.SortBy = "ClientName";
            reportParameter.FileType = fileType;
            reportParameter.GroupBy = "Branch";
            reportParameter.BranchId = 0;
            reportParameter.ServiceTeamId = 0;
            reportParameter.SalesTeamId = 0;

        }
    }
}
