﻿
namespace INSIGHTReportTest.Report
{
    public class UnallocatedCashCreditsReport : ReportTestBase
    {

        public UnallocatedCashCreditsReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/UnallocatedCashCreditsReport";
            reportParameter.FileType = fileType;

            reportParameter.AsAtDate = "2020-04-07";
            reportParameter.AuthorisedRepId=0;
            reportParameter.BranchId=0;
            reportParameter.ClientAutoComplete="(All)";
            reportParameter.ClientId=0;
            reportParameter.CreditsReceivedFromInsurersOnly=false;
            reportParameter.FileType=fileType;
            reportParameter.GroupBy="None";
            reportParameter.GroupPageBreak=false;
            reportParameter.InsurerId=0;
            reportParameter.SalesTeamId=0;
            reportParameter.SelectAllRecords=true;
            reportParameter.ServiceTeamId=0;
            reportParameter.SortBy="Client";
        }
    }
}
