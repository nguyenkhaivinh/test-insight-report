﻿
namespace INSIGHTReportTest.Report
{
    public class ClientPermissionsGroupReport : ReportTestBase
    {

        public ClientPermissionsGroupReport(string fileType)
        {
            ReportRequestUrl = @"/Services/Report/ClientPermissionsGroupReport";
            reportParameter.GroupBy = "ClientPermissionsGroup";
            reportParameter.SelectAllRecords = false;
            reportParameter.FileType = fileType;
            reportParameter.clientPermissionsGroupId = IsAUMode ? 7 : 3;
            reportParameter.SalesTeamId = 0;
            reportParameter.BranchId = 0;
            reportParameter.AuthorisedRepId = 0;

        }
    }
}
