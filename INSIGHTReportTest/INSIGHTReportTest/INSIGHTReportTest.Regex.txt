﻿,\"0\.\d{15}\"$
\"Steadfast Brokers\"
^(\d{1,2}:?)+\s?(A|a|P|p)\.?(M|m)\.?$
^(\d{1,2}\/){2}\d{4}$
^(\d{1,2}\/){2}\d{4}\s(\d{1,2}:?)+\s?(A|P)M$
^\d{1,2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{4}$
^As at (\d{1,2}-){2}\d{4}
^As at\s?: \d{1,2}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-\d{4}
Insurer Statement Report\s+as at
Payable Amount as at :
Run by selenium on
Statement of GST Payable Amount
Statement of Insurer Payable Amount as at :
Total Outstanding as at