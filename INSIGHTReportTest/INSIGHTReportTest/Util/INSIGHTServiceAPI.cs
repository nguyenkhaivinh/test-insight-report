﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Security.Policy;
using System.Text;

namespace INSIGHTReportTest.Util
{
    public class INSIGHTServiceAPI
    {

        public JObject SendRequest(string url, object data)
        {
            url = Config.InsightBaseUrlApi + url;
            HttpWebRequest request = NewWebRequest(url);
            request.Method = "POST";
            if (data != null)
            {
                byte[] block = Encoding.UTF8.GetBytes(
                    JsonConvert.SerializeObject(data)
                );

                request.GetRequestStream().Write(block, 0, block.Length);
            }
            //var content = new StringContent(data.ToString(), Encoding.UTF8, "application/json");
            Exception ex = null;
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    throw;
                }
                response = (HttpWebResponse)e.Response;
                ex = e;
            }
            string text = "";
            if (response != null)
            {
                var reader = new StreamReader(
                    response.GetResponseStream(),
                    Encoding.UTF8
                );

                using (reader)
                {
                    text = reader.ReadToEnd();
                }
            }
            return JObject.Parse(text);
        }
        
        public string DownloadReport(int TaskID, string downloadpath, string reportName, string result = "")
        {
            string url = reportName == "ClientFSGEmailListReport"
                ? string.Format("{0}{1}", Config.InsightBaseUrlApi + "/Services/Report/DownloadTempFile?blobPointer=",
                    result.EndsWith(";") ? result.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0] : result)
                : string.Format("{0}{1}", Config.InsightBaseUrlApi + "/Services/Report/DownloadReportFile?backgroundJobId=", TaskID);
            HttpWebRequest request = NewWebRequest(url);
            request.Method = "GET";
            using (WebResponse response = request.GetResponse())
            using (Stream streamToReadFrom = response.GetResponseStream())
            {
                string filename = reportName + response.Headers["Content-Disposition"].Substring(response.Headers["Content-Disposition"].Length - 4); // response.Headers["Content-Disposition"].Replace("attachment; filename=", "");
                Directory.CreateDirectory(downloadpath);
                string outputfile = Path.Combine(downloadpath, filename);

                using (Stream streamToWriteTo = File.Open(outputfile, FileMode.Create))
                {
                    streamToReadFrom.CopyTo(streamToWriteTo);
                }
                
                return outputfile;
                
            }
        }

        private static HttpWebRequest NewWebRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Headers.Add("INSIGHT-USERNAME", Config.InsightApiUserName);
            request.Headers.Add("INSIGHT-PASSWORD", Config.InsightApiPassword);
            return request;
        }
    }
}
