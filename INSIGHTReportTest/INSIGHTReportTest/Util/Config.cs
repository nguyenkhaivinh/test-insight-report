﻿using System.IO;
using System.Reflection;
using System.Xml.Linq;

namespace INSIGHTReportTest.Util
{
    public static class Config
    {
        private static readonly string _insightBaseApiUrl = ReadAppSetting("InsightLibrary.InsightAPIURL", string.Empty);
        private static readonly string _insightApiUserName = ReadAppSetting("InsightLibrary.InsightAPIUserName", string.Empty);
        private static readonly string _insightApiPassword = ReadAppSetting("InsightLibrary.InsightAPIPassword", string.Empty);
        private static readonly string _ignoreLinesMatchingRegularExpression = ReadAppSetting("IgnoreLinesMatchingRegularExpression", string.Empty);

        private static readonly string _logRootDir = ReadAppSetting("S2.Core.LogRootDir", string.Empty);
        private static readonly string _logRootUrl = ReadAppSetting("S2.Core.LogRootUrl", string.Empty);
        
        private static readonly string _ftpRootUrl = ReadAppSetting("S2.Core.FtpRootUrl", string.Empty);
        private static readonly string _ftpUserName = ReadAppSetting("S2.Core.FtpUsername", string.Empty);
        private static readonly string _ftpPassword = ReadAppSetting("S2.Core.FtpPassword", string.Empty);

        private static readonly string _testRailExBy = ReadAppSetting("S2.Core.TestRailExBy", string.Empty);
        private static readonly string _testRailUrl = ReadAppSetting("S2.Core.TestRailUrl", string.Empty);
        private static readonly string _testRailUserName = ReadAppSetting("S2.Core.TestRailUserName", string.Empty);
        private static readonly string _testRailPassword = ReadAppSetting("S2.Core.TestRailPassword", string.Empty);

        private static readonly string _publicFolder = ReadAppSetting("InsightLibrary.PublicFolder", string.Empty);
        private static readonly string _localFolder = ReadAppSetting("InsightLibrary.LocalFolder", string.Empty);
        private static readonly string _country = ReadAppSetting("InsightLibrary.Country", string.Empty);

        private static XDocument GetXMLDocument()
        {
            string LibraryPath = Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath);
            string fileConfig = LibraryPath + "\\INSIGHTReportTest.dll.config";
            string xmlString = File.ReadAllText(fileConfig);

            return XDocument.Parse(xmlString);
        }

        public static string ReadAppSetting(string name, string defaultValue)
        {
            string setting = "";
            try
            {
                XDocument xdoc = GetXMLDocument();
                var root = xdoc.Element("configuration");
                var appSettings = root.Element("appSettings").Descendants();
                foreach (var appSettingNode in appSettings)
                {
                    string nodename = appSettingNode.Attribute("key").Value;
                    if (nodename == name)
                    {
                        setting = appSettingNode.Attribute("value").Value;
                    }
                }
            }
            catch
            {
                setting = string.Empty;
            }

            return setting;
        }
        public static string InsightBaseUrlApi
        {
            get
            {
                return _insightBaseApiUrl;
            }
        }
        public static string InsightApiUserName
        {
            get
            {
                return _insightApiUserName;
            }
        }
        public static string InsightApiPassword
        {
            get
            {
                return _insightApiPassword;
            }
        }

        public static string IgnoreLinesMatchingRegularExpression
        {
            get
            {
                return _ignoreLinesMatchingRegularExpression;
            }
        }

        /// <summary>
        /// Full path of directory log to contains error screenshot
        /// </summary>
        public static string LogRootDir
        {
            get
            { return _logRootDir; }
        }

        /// <summary>
        /// Full path of virtual directory to contains error screenshot
        /// </summary>
        public static string LogRootUrl
        {
            get
            { return _logRootUrl; }
        }

        /// <summary>
        /// Address of Test Rail Steadfast system
        /// </summary>
        public static string TestRailUrl
        {
            get
            { return _testRailUrl; }
        }

        /// <summary>
        /// User name is to conect to Test Rail Steadfast system
        /// </summary>
        public static string TestRailUserName
        {
            get
            { return _testRailUserName; }
        }

        /// <summary>
        /// Password is to connect to Test Rail Steadfast system
        /// </summary>
        public static string TestRailPassword
        {
            get
            { return _testRailPassword; }
        }

        /// <summary>
        /// FTP address to upload images/screenshots to
        /// </summary>
        public static string FtpRootUrl
        {
            get
            { return _ftpRootUrl; }
        }

        /// <summary>
        /// Username to connect to file server
        /// </summary>
        public static string FtpUsername
        {
            get
            { return _ftpUserName; }
        }

        /// <summary>
        /// Password to connect to file server
        /// </summary>
        public static string FtpPassword
        {
            get
            { return _ftpPassword; }
        }

        /// <summary>
        /// Executed By
        /// </summary>
        public static string TestRailExBy
        {
            get
            {
                return _testRailExBy;
            }
        }

        /// <summary>
        /// Public Folder of compare result
        /// </summary>
        public static string PublicFolder
        {
            get
            {
                return _publicFolder;
            }
        }

        /// <summary>
        /// Local Folder of compare result
        /// </summary>
        public static string LocalFolder
        {
            get
            {
                return _localFolder;
            }
        }

        public static string Country
        {
            get
            {
                return _country;
            }
        }
    }
}
