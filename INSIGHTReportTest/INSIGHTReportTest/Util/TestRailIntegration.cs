﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.Util
{
    public class TestRailIntegration
    {
        protected static TestRailIntegration _testrail;
        protected const string DEFAULT_TEST_PLAN = "";
        protected const string DEFAULT_TEST_PROJECT = "";

        public void InitTestRailProject(string planName, string projectName, IEnumerable<string> suites, string environment, string product_insurer_tag = "")
        {
            try
            {
                ProjectInfo info = new ProjectInfo()
                {
                    TestPlanName = String.IsNullOrEmpty(planName) ? DEFAULT_TEST_PLAN : planName,
                    TestRailProjectName = String.IsNullOrEmpty(projectName) ? DEFAULT_TEST_PROJECT : projectName,
                    TestSuites = suites.Count() == 0 ? DEFAULT_TEST_SUITES.ToList() : suites.ToList(),
                    ExecuteEnvironment = Tuple.Create<string, string>("custom_svu_environment", environment),
                    ProductInsurerTAG = Tuple.Create<string, string>(product_insurer_tag, "Automated"),
                    LogRootUrl = Config.LogRootUrl,
                    TestRailUrl = Config.TestRailUrl,
                    TestRailUserName = Config.TestRailUserName,
                    TestRailPassword = Config.TestRailPassword,
                    FtpRootUrl = Config.FtpRootUrl,
                    FtpUsername = Config.FtpUsername,
                    FtpPassword = Config.FtpPassword
                };
                Console.WriteLine("InitTestRailProject: suites[{0}]", suites.ToDisplayedString());
                _testrail = new TestRailIntegration(info);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
            }
        }
    }
}
