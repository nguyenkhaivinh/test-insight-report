﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using INSIGHTReportTest.Diff;
using INSIGHTReportTest.Report;
using System.Dynamic;
using INSIGHTReportTest.TestRail;
using INSIGHTReportTest.Util;
using TestRailWrapper;
using System.Reflection;
namespace INSIGHTReportTest
{

    public class ReportWorker
    {
        

        private string docsResultDir; // actual folder or download folder
        private string docsBaseDir; // baseline folder

        //private List<ReportTestBase> listDownload;
        public bool Enable2TestRail = false;
        public bool CopyToBaselineIfNotExists = false;

        protected static int _runId = 0;
        protected static IEnumerable<int> _caseIds = new List<int>();

        public static TestRailIntegration _testrail;
        protected const string DEFAULT_TEST_PLAN = "";
        protected const string DEFAULT_TEST_PROJECT = "";
        protected readonly string[] DEFAULT_TEST_SUITES = new string[] { "Regression" };

        Dictionary<string, int> mapCasesId = new Dictionary<string, int>();

        public ProjectInfo ProjectInfo;

        private TestOutcome TestResult = new TestOutcome();

        #region start report
        public void InitDefaultTestRailProject()
        {
            InitTestRailProject(string.Empty, string.Empty, DEFAULT_TEST_SUITES.ToList(), string.Empty);
        }

        public void InitTestRailProject(string planName, string projectName, IEnumerable<string> suites, string environment, string product_insurer_tag = "")
        {
            try
            {
                ProjectInfo info = new ProjectInfo()
                {
                    TestPlanName = String.IsNullOrEmpty(planName) ? DEFAULT_TEST_PLAN : planName,
                    TestRailProjectName = String.IsNullOrEmpty(projectName) ? DEFAULT_TEST_PROJECT : projectName,
                    TestSuites = suites.Count() == 0 ? DEFAULT_TEST_SUITES.ToList() : suites.ToList(),
                    ExecuteEnvironment = Tuple.Create<string, string>("env", environment),
                    //ProductInsurerTAG = Tuple.Create<string, string>(product_insurer_tag, "Automated"),
                    LogRootUrl = Config.LogRootUrl,
                    TestRailUrl = Config.TestRailUrl,
                    TestRailUserName = Config.TestRailUserName,
                    TestRailPassword = Config.TestRailPassword,
                    FtpRootUrl = Config.FtpRootUrl,
                    FtpUsername = Config.FtpUsername,
                    FtpPassword = Config.FtpPassword
                };
                //Console.WriteLine("InitTestRailProject: suites[{0}]", suites.ToDisplayedString());
                 _testrail = new TestRailIntegration(info);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
            }
        }

        /// <summary>
        /// set testrun id from existing to update result
        /// </summary>
        /// <param name="runId"></param>
        public void SetTestRunIdAndGetCaseIds(int runId)
        {
            _runId = runId;
            _caseIds = _testrail.GetCaseIDByTestStatus(runId, new List<int> { 3, 4, 5 }).Select(c => Convert.ToInt32(c));
            Console.WriteLine("Case Ids need to be run: {0}", string.Join("|", _caseIds));
        }

        public void SetCaseIds(List<int> caseIds)
        {
            _caseIds = caseIds;
            Console.WriteLine("Case Ids need to be run: {0}", string.Join("|", _caseIds));
        }

        /// <summary>
        /// Create a test plan with specific name
        /// </summary>
        /// <param name="TestPlanName"></param>
        public IEnumerable<int> CreateTestPlan(string TestPlanName)
        {
            _runId = _testrail.CreateTestPlan(TestPlanName);
            return _testrail.GetCasesIds();
        }


        public void CloseTestPlan()
        {
            _testrail.CloseTestPlan();
        }

        public void ReportResult()
        {
            foreach (var cases in mapCasesId)
            {
                var caseId = cases.Value;
                _testrail.SetTestCaseID(caseId);
                _testrail.SetTestStartTime(DateTime.Now);
                _testrail.SetTestEndTime(DateTime.Now.AddMinutes(1));
                _testrail.SetTestResult(TestResultEnum.Failed);
                _testrail.Report2TestRail(_runId.ToString());
            }
        }

        public void ReportResult(int caseId, bool result)
        {
            if (Enable2TestRail == false) return;
            _testrail.SetTestCaseID(caseId);
            _testrail.SetTestStartTime(DateTime.Now);
            _testrail.SetTestEndTime(DateTime.Now.AddMinutes(1));
            TestResultEnum ret = TestResultEnum.Failed; 
            if (result)
            {
                ret = TestResultEnum.Passed;
            }
            
            _testrail.SetResultReportingData(_runId);
            _testrail.SetTestResult(ret);
            
            _testrail.Report2TestRail(_runId.ToString());
        }



        enum ReportName
        {
            InsurerLiabilityReport,
            InsurerMasterList,
            InsurerPayables,
            InsurerStatement
        }

        /// <summary>
        /// construction: will clear file in Actual folder
        /// </summary>
        public ReportWorker()
        {
            //listDownload = new List<ReportTestBase>();
        }

        public void InitFolders()
        {
            Init(out docsResultDir, out docsBaseDir);
        }

        /// <summary>
        /// Init to prepare folder
        /// </summary>
        /// <param name="docsResultDir"></param>
        /// <param name="docsBaseDir"></param>
        private void Init(out string docsResultDir, out string docsBaseDir)
        {
            string folderToCheck = ReportTestBase.ReportOutputDir;

            docsResultDir = Path.Combine(folderToCheck, "Actual");
            docsBaseDir = Path.Combine(folderToCheck, "Baseline");

            Directory.CreateDirectory(folderToCheck);
            Directory.CreateDirectory(docsResultDir);
            Directory.CreateDirectory(docsBaseDir);

            if (!Directory.Exists(docsBaseDir))
            {
                throw new Exception(string.Format("DocsBaseDir[{0}] does not exist. Please check...", docsBaseDir));
            }

            if (Directory.GetFiles(docsBaseDir).Length == 0 && !CopyToBaselineIfNotExists)
            {
                throw new Exception(string.Format("DocsBaseDir[{0}] has got no file. Please check...", docsBaseDir));
            }

            DirectoryInfo resultDirsInfo = new DirectoryInfo(docsResultDir);
            resultDirsInfo.CreateIfNotExisted();
            //resultDirsInfo.DeleteAllFiles();
        }

        /// <summary>
        /// Compare Folder
        /// </summary>
        /// <returns></returns>
        public bool Compare()
        {
            //dataBridgeHandler.GetAllProductAttachmentDocuments(DocsResultDir);
            List<string> files = Directory.EnumerateFiles(docsBaseDir).Select(f => Path.GetFileName(f)).ToList();

            TimeSpan ts = TimeSpan.FromSeconds(30);
           
            //foreach (string file in files)
            //{
            //    Task.Factory.StartNew(() =>
            //    {
            //        string fullFilePath = Path.Combine(PATH_DOWNLOADS_AUTO, file);
            //        string destPath = Path.Combine(docsResultDir, file);
            //        File.Move(fullFilePath, destPath);

            //    }).Wait();
            //}


            string output = string.Empty;
            bool ret = DiffWorker.CompareDirectory(docsBaseDir, docsResultDir, ref output);
            
            return ret;
            //downloadsFolder.DeleteAllFiles();
            //return false;
        }

        /// <summary>
        /// compare file
        /// </summary>
        /// <param name="fileBaseline"></param>
        /// <param name="fileActual"></param>
        /// <param name="caseId"></param>
        public void CompareFile(string fileBaseline, string fileActual, int caseId)
        {
            var res = DiffWorker.ComparePDFFiles(fileBaseline, fileActual, ReportTestBase.ReportOutputDir + "\\Result", Path.GetFileName(fileBaseline));
            ReportResult(caseId, res); 
        }

#endregion

        #region DOWNLOAD REPORT
        /// <summary>
        /// this is just used for download all report
        /// </summary>
        /// <param name="baselinePath">if it has value, we will copy to the baselinePath</param>
        public void Download(string baselinePath = "")
        {
            //DownloadInsurerMasterListReport();
            //DownloadInsurerPayablesReport();
            //DownloadInsurerStatementReport();
            //DownloadAPRAForm701Report();
            //DownloadClientFSGEmailListReport();
            //DownloadFSGNotSentReport();
            //DownloadSteadfastReport();
            //DownloadAuthorisedRepLiabilityReport();
            //DownloadAuthorisedRepPayablesReport();
        }

        public T CreateInstance<T>(params object[] paramArray)
        {
            return (T)Activator.CreateInstance(typeof(T), args: paramArray);
        }

        /// <summary>
        /// to download a report by Name with a test case Id to update result
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="reportName"></param>
        /// <param name="reporttype">PDF/XLS</param>
        public void DownloadAndCompare(int caseId, string reportName, string fileType = "PDF")
        {
            if (!_caseIds.Contains(caseId))
            {
                Console.WriteLine("Ignore caseId: {0}", caseId);
                return;
            }

            _testrail.AppendTestMessage(string.Format("INSIGHT Broker Portal: '{0}'", Config.InsightBaseUrlApi));
            _testrail.AppendTestMessage(string.Format("caseId[{0}], reportName[{1}], fileType[{2}]", caseId, reportName, fileType));

            Type thisType = this.GetType();
            MethodInfo theMethod = thisType.GetMethod("Download" + reportName);
            var objParams = new string[1] {"PDF"};
            if (fileType.ToLower().Equals("xls"))
            {
                objParams[0] = "MsExcel";
            }
            //theMethod.Invoke(this, objParams);

            string objectToInstantiate = "INSIGHTReportTest.Report." + reportName;
            var objectType = Type.GetType(objectToInstantiate);
            ReportTestBase myReport = Activator.CreateInstance(objectType, objParams) as ReportTestBase;
            myReport.GenerateAndDownloadReport();

            string fullReportName = Path.GetFileName(myReport.ReportOutputFile);

            string fileBaseline = Path.Combine(docsBaseDir, fullReportName);
            string fileActual = Path.Combine(docsResultDir, fullReportName);
            if (!File.Exists(fileBaseline) && File.Exists(fileActual) && CopyToBaselineIfNotExists)
            {
                File.Copy(fileActual, fileBaseline);
            }
            
            _testrail.AppendTestMessage(string.Format("Baseline: {0}", fileBaseline));
            _testrail.AppendTestMessage(string.Format("Actual: {0}", fileActual));
            if (!File.Exists(fileActual))
            {
                ReportResult(caseId, false);
                return;
            }
            CompareFile(fileBaseline, fileActual, caseId);
        }

        /*
        public void DownloadABNMissingReport(string fileType = "PDF")
        {
            ABNMissingReport report = new ABNMissingReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadInsurerLiabilityReport(string fileType)
        {
            var report = new InsurerLiabilityReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadInsurerMasterListReport(string fileType = "PDF")
        {
            InsurerMasterListReport report = new InsurerMasterListReport(fileType);
            
            report.GenerateAndDownloadReport();
        }

        public void DownloadInsurerPayablesReport(string fileType = "PDF")
        {
            InsurerPayablesReport report = new InsurerPayablesReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadInsurerStatementReport(string fileType = "PDF")
        {
            InsurerStatementReport report = new InsurerStatementReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadAPRAForm701Report(string fileType = "PDF")
        {
            APRAForm701Report report = new APRAForm701Report(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClientFSGEmailListReport(string fileType = "PDF")
        {
            ClientFSGEmailListReport report = new ClientFSGEmailListReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadFSGNotSentReport(string fileType = "PDF")
        {
            FSGNotSentReport report = new FSGNotSentReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadSteadfastReport(string fileType = "PDF")
        {
            SteadfastReport report = new SteadfastReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadAuthorisedRepLiabilityReport(string fileType = "PDF")
        {
            AuthorisedRepLiabilityReport report = new AuthorisedRepLiabilityReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadAuthorisedRepPayablesReport(string fileType = "PDF")
        {
            AuthorisedRepPayablesReport report = new AuthorisedRepPayablesReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadNetAuthorisedRepStatementReport(string fileType = "PDF")
        {
            NetAuthorisedRepStatementReport report = new NetAuthorisedRepStatementReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClaimReport(string fileType = "PDF")
        {
            ClaimReport report = new ClaimReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClientAnalysisByAreaReport(string fileType = "PDF")
        {
            ClientAnalysisByAreaReport report = new ClientAnalysisByAreaReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClientAnalysisByIndustryReport(string fileType = "PDF")
        {
            ClientAnalysisByIndustryReport report = new ClientAnalysisByIndustryReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClientMasterListReport(string fileType = "PDF")
        {
            ClientMasterListReport report = new ClientMasterListReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadBatchClientNotesReport(string fileType = "PDF")
        {
            BatchClientNotesReport report = new BatchClientNotesReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadIndividualClientNotesReport(string fileType = "PDF")
        {
            IndividualClientNotesReport report = new IndividualClientNotesReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClientPermissionsGroupReport(string fileType = "PDF")
        {
            ClientPermissionsGroupReport report = new ClientPermissionsGroupReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadClientStatementReport(string fileType = "PDF")
        {
            ClientStatementReport report = new ClientStatementReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadMailMergeClientsReport(string fileType = "PDF")
        {
            MailMergeClientsReport report = new MailMergeClientsReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadEarningsReport(string fileType = "PDF")
        {
            EarningsReport report = new EarningsReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadEarningsSummaryReport(string fileType = "PDF")
        {
            EarningsSummaryReport report = new EarningsSummaryReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadPortfolioAnalysisReport(string fileType = "PDF")
        {
            PortfolioAnalysisReport report = new PortfolioAnalysisReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadBusinessWrittenReport(string fileType = "PDF")
        {
            BusinessWrittenReport report = new BusinessWrittenReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadASICReturnsReport(string fileType = "PDF")
        {
            ASICReturnsReport report = new ASICReturnsReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadBankReconciliationReport(string fileType = "PDF")
        {
            BankReconciliationReport report = new BankReconciliationReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadDrawingsReport(string fileType = "PDF")
        {
            DrawingsReport report = new DrawingsReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadGLAccountsReport(string fileType = "PDF")
        {
            GLAccountsReport report = new GLAccountsReport(fileType);
            report.GenerateAndDownloadReport();
        }

        public void DownloadGLJournalReport(string fileType = "PDF")
        {
            GLJournalReport report = new GLJournalReport(fileType);
            report.GenerateAndDownloadReport();
        }*/

        #endregion
    }
}
