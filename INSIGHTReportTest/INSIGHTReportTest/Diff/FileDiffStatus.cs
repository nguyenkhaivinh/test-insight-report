﻿

namespace INSIGHTReportTest.Diff
{
    public enum FileDiffStatus : int
    {
        Different,
        Identical,
        OnlyInBase,
        OnlyInResult
    }
}
