﻿using INSIGHTReportTest.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.Diff
{
    public class DiffWorker
    {

        protected static readonly string _libraryPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().Location);


        /// <summary>
        /// constructor 
        public DiffWorker()
        {

        }

        /// <summary>
        /// Compare Directory that contains list of pdf files
        /// </summary>
        /// <param name="baseDirPath"></param>
        /// <param name="resultDirPath"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public static bool CompareDirectory(string baseDirPath, string resultDirPath, ref string output)
        {
            bool ret = false;
            ExamDiff.Options _option = new ExamDiff.Options();
            _option.DiffFilePath = Path.Combine(resultDirPath, "DirectoryDifferences.txt");
            _option.IgnoreLinesMatchingRegularExpression = GetIgnoreLinesMatchingRegularExpression();
            
            // 23/11/2016 Lin Shen
            // Add commadline option to load the Configurations, since in Jenkins, the account of executing test is system account
            // and the setting of examdiff is associated with current OS account only.

            //Change the regular express since loading options from file will overwrite the setting from commandline
            //string ExamDiffOptions = Path.Combine(_libraryPath, "ExamdiffOptions.txt");
            //string optionsContent = File.ReadAllText(ExamDiffOptions);

            //Regex rgx = new Regex("Regular expression hex=+.*");
            //optionsContent = rgx.Replace(optionsContent, "Regular expression hex=" + SVUConfig.IgnoreLinesMatchingRegularExpression);
            //File.WriteAllText(ExamDiffOptions, optionsContent);

            //_option.CommandLineOptions += " /g:" + ExamDiffOptions;
            ExamDiff examDiff = new ExamDiff(_option);

            int res = examDiff.Diff(baseDirPath, resultDirPath);

            switch (res)
            {
                case 0:
                    output = "All files are unchanged";
                    ret = true;

                    break;
                case 2:
                    output = "An error occured\n";
                    break;
                case 1:
                    List<FileDiff> filesDiff = examDiff.GetAllFilesDiff(_option.IgnoreLinesMatchingRegularExpression);
                    StringBuilder sb = new StringBuilder();
                    filesDiff.ForEach(f =>
                    {
                        sb.AppendLine(f.ToString());
                    });

                    output = sb.ToString();

                    break;
            }
            return ret;
        }

        public static bool ComparePDFFiles(string fileToCheck, string fileActual, string resultDir, string fileResult)
        {
            ExamDiff.Options _option = new ExamDiff.Options(true);
            string fileDiff = fileResult.Replace(".pdf", string.Empty).Replace(" ", string.Empty) + "_Differences.html";
            _option.DiffFilePath = Path.Combine(resultDir, fileDiff);
            string regexes = GetIgnoreLinesMatchingRegularExpression();
            _option.IgnoreLinesMatchingRegularExpression = regexes;
            ExamDiff examDiff = new ExamDiff(_option);
            fileToCheck = string.Format("\"{0}\"", fileToCheck);
            fileActual = string.Format("\"{0}\"", fileActual);
            //var re = string.Join("|", IgnoreRegEx.Values);
            int res = examDiff.Diff(fileToCheck, fileActual);
            //GeneralKeywords.TestRail.AppendTestMessage(string.Format("***** Doc Compared File: {0}", _option.DiffFilePath));
            switch (res)
            {
                case 0:
                    var output0 = "All files are unchanged";
                    ReportWorker._testrail.AppendTestMessage(output0);
                    return true;
                case 2:
                    var output2 = "An error occured\n. Such as missing file actual or baseline.";
                    ReportWorker._testrail.AppendTestMessage(output2);
                    return false;
                case 1:
                    try
                    {
                        ReportWorker._testrail.AppendTestMessage(string.Format("***** Please check diff in file: {0}", _option.DiffFilePath));
                        //GeneralKeywords.TestRail.AppendTestMessage(string.Format("***** Please check diff in file: {0}", _option.DiffFilePath));
                        /*List<FileDiff> filesDiff = examDiff.GetAllFilesDiff(_option.IgnoreLinesMatchingRegularExpression);
                        StringBuilder sb = new StringBuilder();
                        filesDiff.ForEach(f =>
                        {
                            sb.AppendLine(f.ToString());
                        });

                        var output1 = sb.ToString();
                        GeneralKeywords.TestRail.AppendTestMessage(output1);*/
                        return false;
                    }
                    catch (KeyNotFoundException)
                    {
                        return false;
                    }
            }
            //GeneralKeywords.TestRail.AppendTestMessage(string.Format("IN PROGRESS: MANUAL QC WILL CHECK THE RESULT"));
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static string GetIgnoreLinesMatchingRegularExpression()
        {
            //return string.Empty;
            string regexFile = Path.Combine(_libraryPath, string.Format("{0}.Regex.txt", Assembly.GetCallingAssembly().GetName().Name));
            if (File.Exists(regexFile))
            {
                List<string> lines = File.ReadAllLines(regexFile)
                    .Where(line => !string.IsNullOrEmpty(line))
                    .ToList();

                lines = lines.Select(line => string.Format("({0})", line)).ToList();

                return string.Join("|", lines);
            }
            else
            {
                return Config.IgnoreLinesMatchingRegularExpression;
            }
        }


    }
}
