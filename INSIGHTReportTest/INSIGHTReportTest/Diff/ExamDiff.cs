﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace INSIGHTReportTest.Diff
{
    public class ExamDiff
    {

        public class Options
        {
            public Options(bool file = false)
            {
                IgnoreLinesMatchingRegularExpression = "";
                DiffFilePath = "";
                // use this options to compare folder
                if (file == false)
                {
                    CommandLineOptions = "/nh /c /l /e /b /k /f";
                }
                else
                {
                    // use this options to compare file
                    CommandLineOptions = "/nh /c /l /e /b /k /html";
                }
            }

            /// <summary>
            /// Ignore lines matching regular expression
            /// </summary>
            public string IgnoreLinesMatchingRegularExpression { get; set; }

            /// <summary>
            /// Diff file path
            /// </summary>
            public string DiffFilePath { get; set; }

            /// <summary>
            /// Command line options
            /// </summary>
            public string CommandLineOptions { get; set; }
        }

        private Options _options;
        private int _exitCode;

        private const string _identicalFiles = "Identical files";
        private const string _differentFiles = "Different files";
        private const string _onlyInFirstDirectory = "Only in (first)";
        private const string _onlyInSecondDirectory = "Only in (second)";

        private static IDictionary<string, FileDiffStatus> _dictFileDiffStatuses = new Dictionary<string, FileDiffStatus>()
        {
            {_identicalFiles, FileDiffStatus.Identical},
            {_differentFiles, FileDiffStatus.Different},
            {_onlyInFirstDirectory, FileDiffStatus.OnlyInBase},
            {_onlyInSecondDirectory, FileDiffStatus.OnlyInResult}
        };

        public ExamDiff(Options options)
        {
            _options = options;
        }

        public Options Option
        {
            get
            {
                return _options;
            }
            set
            {
                _options = value;
            }
        }


        /// <summary>
        /// Diff between two directories
        /// </summary>
        /// <param name="sourceFileOrDirPath">Source file or directory to compare</param>
        /// <param name="destFileOrDirPath">Destination file or directory to compare</param>
        /// <returns>0 identical files or directories
        /// 1 different files or directories
        /// 2 an error occured
        /// </returns>
        public int Diff(string sourceFileOrDirPath, string destFileOrDirPath)
        {
            if (string.IsNullOrEmpty(sourceFileOrDirPath) ||
                string.IsNullOrEmpty(destFileOrDirPath))
            {
                throw new ArgumentNullException("Provide target directory to compare files");
            }

            string examDiffArguments = String.Concat("/n ", sourceFileOrDirPath + " ", destFileOrDirPath + " ", _options.CommandLineOptions, " ");
            string examDiffArgumentsOptions = "";

            if (_options != null)
            {
                examDiffArgumentsOptions += "/o:" + _options.DiffFilePath + " ";
                if (!string.IsNullOrEmpty(_options.IgnoreLinesMatchingRegularExpression))
                {
                    examDiffArgumentsOptions += "/j:" + "\"" + _options.IgnoreLinesMatchingRegularExpression + "\"";
                }
            }

            Process examDiffProcess = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "examdiff.exe",
                    Arguments = examDiffArguments + examDiffArgumentsOptions,
                }
            };

            examDiffProcess.Start();
            examDiffProcess.WaitForExit();
            _exitCode = examDiffProcess.ExitCode;
            examDiffProcess.Close();

            return _exitCode;
        }

        /// <summary>
        /// Get all the files different in comparison (exit code = 1)
        /// </summary>
        /// <returns></returns>
        public List<FileDiff> GetAllFilesDiff(string ignoreLinesMatchingRegularExpression)
        {
            List<FileDiff> filesDiff = new List<FileDiff>();

            if (_exitCode != 1)
            {
                return filesDiff;
            }

            using (StreamReader reader = new StreamReader(_options.DiffFilePath))
            {
                string line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    string fileDiffStatus = "";
                    string[] inputs = line.Split(new string[] { " " }, StringSplitOptions.None);
                    try
                    {
                        if (line.StartsWith(_identicalFiles) || line.StartsWith(_differentFiles))
                        {
                            fileDiffStatus = inputs[0] + " " + inputs[1];
                        }
                        else
                        {
                            fileDiffStatus = inputs[0] + " " + inputs[1] + " " + inputs[2];
                        }
                    

                        FileDiff fileDiff = new FileDiff();
                        switch (_dictFileDiffStatuses[fileDiffStatus])
                        {
                            case FileDiffStatus.Identical:
                                fileDiff = FileDiff.Factory.CreateFileDiff(inputs, FileDiffStatus.Identical);
                                break;

                            case FileDiffStatus.Different:
                                fileDiff = FileDiff.Factory.CreateFileDiff(inputs, FileDiffStatus.Different);

                                // Generate output diff file
                                _options.CommandLineOptions = "/nh /c /l /e /b /k /html";
                                _options.DiffFilePath = fileDiff.Url;
                                _options.IgnoreLinesMatchingRegularExpression = ignoreLinesMatchingRegularExpression;

                                Diff(fileDiff.SourceFilePath, fileDiff.DestinationFilePath);
                                break;

                            case FileDiffStatus.OnlyInBase:
                                fileDiff = FileDiff.Factory.CreateFileDiff(inputs, FileDiffStatus.OnlyInBase);
                                break;

                            case FileDiffStatus.OnlyInResult:
                                fileDiff = FileDiff.Factory.CreateFileDiff(inputs, FileDiffStatus.OnlyInResult);
                                break;
                            default:
                                break;
                        }

                        filesDiff.Add(fileDiff);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        break;
                    }
                }
            }

            return filesDiff;
        }
    }
}
