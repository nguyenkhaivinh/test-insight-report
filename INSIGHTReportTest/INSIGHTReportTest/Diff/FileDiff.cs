﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace INSIGHTReportTest.Diff
{
    [Serializable]
    public class FileDiff
    {
        /// <summary>
        /// Source file path
        /// </summary>
        public string SourceFilePath
        { get; set; }

        /// <summary>
        /// Destination file path
        /// </summary>
        public string DestinationFilePath
        { get; set; }

        /// <summary>
        /// Status of file diff
        /// </summary>
        public FileDiffStatus Status
        { get; set; }

        /// <summary>
        /// Url of file diff
        /// </summary>
        public string Url
        { get; set; }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            string sourceFileName = "";
            string destFileName = "";

            if (!string.IsNullOrEmpty(SourceFilePath))
            {
                sourceFileName = Path.GetFileName(SourceFilePath);
            }

            if (!string.IsNullOrEmpty(DestinationFilePath))
            {
                destFileName = Path.GetFileName(DestinationFilePath);
            }

            if (Status == FileDiffStatus.Different || Status == FileDiffStatus.Identical)
            {
                sb.AppendFormat("{0} ", sourceFileName);
            }
            else if (Status == FileDiffStatus.OnlyInBase)
            {
                sb.AppendFormat("{0} ", sourceFileName);
            }
            else if (Status == FileDiffStatus.OnlyInResult)
            {
                sb.AppendFormat("{0} ", destFileName);
            }

            sb.AppendFormat("{0} ", Status.ToString());

            return sb.ToString();
        }

        public static class Factory
        {
            /// <summary>
            /// CreateFileDiff
            /// </summary>
            /// <param name="inputs">
            /// Array of string
            /// [FileDiffStatus] -> [SourceFilePath] *and* [DestinationFilePath]
            /// [FileDiffStatus] [SourceFilePath] -> [FileName]
            /// [FileDiffStatus] [DestinationFilePath] -> [FileName]
            /// </param>
            /// <param name="fileDiffStatus"></param>
            /// <returns></returns>
            public static FileDiff CreateFileDiff(string[] inputs, FileDiffStatus fileDiffStatus)
            {
                if (inputs == null || inputs.Count() == 0)
                {
                    throw new ArgumentException("Provide array of string of line exam diff output file");
                }

                FileDiff fileDiff = new FileDiff();

                if (fileDiffStatus == FileDiffStatus.Different
                    || fileDiffStatus == FileDiffStatus.Identical)
                {
                    string sourceFilePath = inputs[3];
                    string destFilePath = inputs[5];

                    string fileName = Path.GetFileName(destFilePath);

                    fileDiff.Url = destFilePath.Replace(fileName, "") + fileName + ".Diff.html";
                    fileDiff.SourceFilePath = sourceFilePath;
                    fileDiff.DestinationFilePath = destFilePath;

                }
                else if (fileDiffStatus == FileDiffStatus.OnlyInBase
                    || fileDiffStatus == FileDiffStatus.OnlyInResult)
                {
                    string filePath = inputs[3];
                    string fileName = string.Join(" ", inputs, 5, inputs.Length - 5);

                    if (fileDiffStatus == FileDiffStatus.OnlyInBase)
                    {
                        fileDiff.SourceFilePath = filePath + "\\" + fileName;
                    }
                    else if (fileDiffStatus == FileDiffStatus.OnlyInResult)
                    {
                        fileDiff.DestinationFilePath = filePath + "\\" + fileName;
                    }

                }

                fileDiff.Status = fileDiffStatus;

                return fileDiff;
            }
        }
    }
}
