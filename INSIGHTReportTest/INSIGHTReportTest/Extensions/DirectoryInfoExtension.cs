﻿using System.IO;
using System.Linq;

namespace INSIGHTReportTest
{
    public static class DirectoryInfoExtension
    {
        public static void CreateIfNotExisted(this DirectoryInfo dirInfo)
        {
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        public static void DeleteAllFiles(this DirectoryInfo dirInfo)
        {
            FileInfo[] files = dirInfo.GetFiles();
            if (files.Any())
            {
                foreach (var file in files)
                {
                    file.Delete();
                }
            }
        }
    }
}
