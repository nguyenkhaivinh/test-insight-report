﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.TestRail
{
    public enum TestRailCaseType : int
    {
        Automated,
        Functionality,
        Migration,
        Performance,
        Printing,
        Regression,
        ToBeAutomated,
        UI,
        Usability
    }
}
