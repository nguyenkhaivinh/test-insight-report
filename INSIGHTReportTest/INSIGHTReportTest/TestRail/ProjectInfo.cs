﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.TestRail
{
    public class ProjectInfo
    {
        public string TestRailProjectName { get; set; }
        public string TestPlanName { get; set; }
        public Tuple<string, string> ExecuteEnvironment { get; set; }
        public string FtpRootUrl { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public string LogRootUrl { get; set; }
        public string TestRailUrl { get; set; }
        public string TestRailUserName { get; set; }
        public string TestRailPassword { get; set; }
        public IEnumerable<string> TestSuites { get; set; }
    }
}
