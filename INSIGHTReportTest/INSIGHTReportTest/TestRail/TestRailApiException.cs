﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.TestRail
{
    [Serializable]
    public class APIException : Exception
    {
        public APIException()
        {
        }

        public APIException(string message)
            : base(message)
        {
        }

        public APIException(string message,
            Exception innerException)
            : base(message, innerException)
        {
        }

        protected APIException(SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}
