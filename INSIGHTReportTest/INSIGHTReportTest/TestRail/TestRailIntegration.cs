﻿using INSIGHTReportTest.Util;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TestRailWrapper;

namespace INSIGHTReportTest.TestRail
{
    public class TestRailIntegration
    {
        public ProjectInfo ProjectInfo;
        private TestRailOperation _testrailOperation;

        private TestOutcome TestResult = new TestOutcome();

        public TestRailIntegration()
        {
        }

        public TestRailIntegration(string svuConfigPath, string TestPlanName, string TestRailProjectName, IEnumerable<string> TestSuites)
        {
            ProjectInfo = new ProjectInfo()
            {
                TestRailProjectName = TestRailProjectName,
                TestPlanName = TestPlanName,
                TestSuites = TestSuites,
                LogRootUrl = _readAppSetting(svuConfigPath, "S2.Core.LogRootUrl"),
                TestRailUrl = _readAppSetting(svuConfigPath, "S2.Core.TestRailUrl"),
                TestRailUserName = _readAppSetting(svuConfigPath, "S2.Core.TestRailUserName"),
                TestRailPassword = _readAppSetting(svuConfigPath, "S2.Core.TestRailPassword"),
                FtpRootUrl = _readAppSetting(svuConfigPath, "S2.Core.FtpRootUrl"),
                FtpUsername = _readAppSetting(svuConfigPath, "S2.Core.FtpUsername"),
                FtpPassword = _readAppSetting(svuConfigPath, "S2.Core.FtpPassword")
            };
            _testrailOperation = new TestRailOperation(ProjectInfo.TestRailUrl, ProjectInfo.TestRailUserName, ProjectInfo.TestRailPassword, ProjectInfo.TestRailProjectName);
        }

        public TestRailIntegration(ProjectInfo projectInfo)
        {
            ProjectInfo = projectInfo;
            _testrailOperation = new TestRailOperation(ProjectInfo.TestRailUrl, ProjectInfo.TestRailUserName, ProjectInfo.TestRailPassword, ProjectInfo.TestRailProjectName);
        }

        /// <summary>
        /// Initializes Test Rail Integration without project information
        /// </summary>
        /// <param name="url">Test Rail URL</param>
        /// <param name="username">Test Rail UserName</param>
        /// <param name="password">Test Rail Password</param>
        public TestRailIntegration(string url, string username, string password)
        {
            _testrailOperation = new TestRailOperation(url, username, password, "UnderwriterCentral");
        }

        public IEnumerable<int> GetCaseIDByTestStatus(int runID, List<int> statusIDs)
        {
            return _testrailOperation.GetCaseIDByTestStatus(runID, statusIDs);
        }

        public void ReportResult(string libraryPath, IWebDriver webdriver, IList<string> tags, string testName, string status, string message)
        {
            // Get caseID
            int caseID = 12345;

            // Set the test case finished time
            SetTestEndTime(DateTime.Now);

            // Set executed by
            string exBy = Config.TestRailExBy;
            SetExBy(exBy);

            //string LibraryPath = Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath);

            // Report test case execution result to TestRail
            if (GetPlanID() != -1)
            {
                if (caseID != 0)
                {
                    SetTestCaseID(caseID);
                    if (status.ToLower() == "fail")
                    {
                        SetTestResult(TestResultEnum.Failed);
                        AppendTestMessage(string.Format("Message from Robot: {0}", message));
                        // take snapshot and upload to server
                        //Snapshot snapshoter = new Snapshot(webdriver, ProjectInfo);//SeleniumContext.Instance.Browser.Driver);

                        //string remoteImgURL = snapshoter.TakeSnapshot(libraryPath + "\\snapshot.png");
                        //AppendTestMessage(string.Format("\\n![]({0})", remoteImgURL));
                    }
                    else
                    {
                        SetTestResult(TestResultEnum.Passed);
                    }
                    Report2TestRail();
                }
            }
            else
            {
                Console.WriteLine("[TestRail] No Need to Report Result.");
            }
        }



        public int CreateTestPlan(string TestPlanName = "")
        {
            if (String.IsNullOrEmpty(TestPlanName))
            {
                TestPlanName = ProjectInfo.TestPlanName;
            }
            //_testrailOperation.CreateTestPlan(TestPlanName, ProjectInfo.TestSuites.ToList());
            //_testrail.CreateTestPlanWithFilter(TestPlanName, ProjectInfo.TestSuites.ToList(), new List<string> { "4", "3", "1" }, ProjectInfo.ExecuteEnvironment);
            return _testrailOperation.CreateTestPlanWithFilter(TestPlanName, ProjectInfo.TestSuites.ToList(), new List<string> { "4", "3", "1" }, ProjectInfo.ExecuteEnvironment);
            //return _testrailOperation.CaseIds;
        }

        public IEnumerable<int> GetCasesIds()
        {
            return _testrailOperation.CaseIds;
        }

        public void CloseTestPlan()
        {
            _testrailOperation.CloseTestPlan();
        }

        public void Report2TestRail()
        {
            try
            {
                _testrailOperation.ReportResult(TestResult);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                TestResult.ClearTestOutcome();
            }

        }

        public void Report2TestRail(string runId = null)
        {
            try
            {
                if (string.IsNullOrEmpty(runId))
                {
                    _testrailOperation.ReportResult(TestResult);
                }
                else
                {
                    _testrailOperation.ReportResultWithRunID(TestResult, Convert.ToInt32(runId));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                TestResult.ClearTestOutcome();
            }
        }

        public int GetPlanID()
        {
            return _testrailOperation._TestRailPlanID;
        }

        public string GetTestProjectName()
        {
            return ProjectInfo.TestRailProjectName;
        }
        public string GetTestPlanName()
        {
            return ProjectInfo.TestPlanName;
        }

        /// <summary>
        /// Retrieves a Test Rail plan
        /// </summary>
        /// <param name="planId">Test Rail plan ID</param>
        public PlanDTO GetTestPlan(int planId)
        {
            return _testrailOperation.GetTestPlan(planId);
        }

        /// <summary>
        /// Retrieves a Test Rail project
        /// </summary>
        /// <param name="projectId">Test Rail project ID</param>
        public ProjectDTO GetTestProject(int projectId)
        {
            return _testrailOperation.GetTestProject(projectId);
        }

        public void AppendTestMessage(string message)
        {
            string formatedMessage = string.Format("\n{0}: {1}", DateTime.Now.ToString(), message);

            Console.WriteLine(formatedMessage);

            TestResult.Message += _NormalizeJsonString(formatedMessage);
        }

        public void AppendTestMessage(ValidationResult result, string stepName = "Current step")
        {
        //    string formatedMessage = string.Format("\n{0}: {1}", DateTime.Now.ToString(), "Validation: " + stepName);
        //    if (result == null)
        //    {
        //        formatedMessage += " - Validation Result is empty";
        //        goto appendResult;
        //    }
        //    if (result.IsValid)
        //        formatedMessage += " - Validation Passed\n";
        //    else
        //    {
        //        formatedMessage += " - VALIDATION FAILED\n";
        //        formatedMessage += string.Join("\n", result.Errors);
        //    }

        //appendResult:
        //    Console.WriteLine(formatedMessage);

        //    TestResult.Message += _NormalizeJsonString(formatedMessage);
        }

        public void SetTestPlan(int id)
        {
            _testrailOperation.SetTestPlan(id);
        }

        public void SetTestProject(int id)
        {
            _testrailOperation.SetTestProject(id);
        }

        public void SetResultReportingData(int runId)
        {
            _testrailOperation.SetResultReportingData(runId);
        }

        public void SetTestStartTime(DateTime start)
        {
            TestResult.CaseStartTime = start;
        }

        public void SetTestEndTime(DateTime end)
        {
            TestResult.CaseEndTime = end;
        }

        public void SetTestResult(TestResultEnum ret)
        {
            TestResult.TestResult = ret;
        }

        public void SetTestCaseID(int caseID)
        {
            TestResult.TestCaseID = caseID;
        }

        public void SetExBy(string exBy)
        {
            TestResult.ExBy = exBy;
        }

        private XDocument _getXMLDocument(string configFilePath)
        {
            string xmlString = System.IO.File.ReadAllText(configFilePath);

            return XDocument.Parse(xmlString);
        }

        private string _readAppSetting(string configFilePath, string name, string defaultValue = "")
        {
            string setting = "";
            try
            {
                XDocument xdoc = _getXMLDocument(configFilePath);
                var root = xdoc.Element("configuration");
                var appSettings = root.Element("appSettings").Descendants();
                foreach (var appSettingNode in appSettings)
                {
                    string nodename = appSettingNode.Attribute("key").Value;
                    if (nodename == name)
                    {
                        setting = appSettingNode.Attribute("value").Value;
                    }
                }
            }
            catch
            {
                setting = string.Empty;
            }

            return setting;
        }

        private string _NormalizeJsonString(string MessageString)
        {
            string ret = string.Empty;

            //replace " and \
            ret = MessageString.Replace("\"", "'").Replace("\\", "/");

            return ret;
        }
    }
}
