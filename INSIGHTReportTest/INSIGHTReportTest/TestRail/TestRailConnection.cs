﻿using INSIGHTReportTest.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.TestRail
{
    public class TestRailConnection
    {
        APIClient _client = new APIClient(Config.TestRailUrl);

        private TestRailConnection()
        {
            _client.User = Config.TestRailUserName;
            _client.Password = Config.TestRailPassword;
        }

        private static Lazy<TestRailConnection> lazy = new Lazy<TestRailConnection>(() => new TestRailConnection());

        public static TestRailConnection Instance
        {
            get { return lazy.Value; }
        }

        public APIClient Client { get { return lazy.Value._client; } }
    }
}
