﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.TestRail
{
    /// <summary>
    /// Identifies the Test Rail Id for a test method
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class TestRailCaseAttribute : Attribute
    {
        /// <summary>
        /// Test case id in the test rail system
        /// </summary>
        private int _id;

        public TestRailCaseAttribute(int id)
        {
            _id = id;
        }

        public int Id { get { return _id; } }

        private bool _isPrinting = false;

        public bool IsPrinting
        {
            get
            {
                return _isPrinting;
            }
            set
            {
                _isPrinting = value;
            }
        }

    }
}
