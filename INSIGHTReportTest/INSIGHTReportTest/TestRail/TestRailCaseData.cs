﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSIGHTReportTest.TestRail
{
    public class TestRailCaseData
    {
        readonly IDictionary<string, object> _dataBuilder = new Dictionary<string, object>();

        private TestRailCaseData() { }

        public int TestRailCaseId
        { get; private set; }

        public int TestRailCaseType
        { get; private set; }

        public string CustomAutomatedTestSuite
        { get; private set; }

        public IDictionary<string, object> ToDictionary()
        {
            _dataBuilder.Add("id", TestRailCaseId);
            _dataBuilder.Add("type_id", TestRailCaseType);
            _dataBuilder.Add("custom_automated_test_suite", CustomAutomatedTestSuite);
            return _dataBuilder;
        }

        public class Builder
        {
            private TestRailCaseData _testRailCaseData = new TestRailCaseData();

            public int TestRailCaseId
            {
                get
                {
                    return _testRailCaseData.TestRailCaseId;
                }
                set
                {
                    _testRailCaseData.TestRailCaseId = value;
                }
            }

            public int TestRailCaseType
            {
                get
                {
                    return _testRailCaseData.TestRailCaseType;
                }
                set
                {
                    _testRailCaseData.TestRailCaseType = value;
                }
            }

            public string CustomAutomatedTestSuite
            {
                get
                {
                    return _testRailCaseData.CustomAutomatedTestSuite;
                }
                set
                {
                    _testRailCaseData.CustomAutomatedTestSuite = value;
                }
            }

            public TestRailCaseData Build()
            {
                TestRailCaseData ret = _testRailCaseData;
                _testRailCaseData = null;
                return ret;
            }

        }

    }
}
