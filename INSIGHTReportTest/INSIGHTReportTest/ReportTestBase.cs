﻿using INSIGHTReportTest.Model;
using INSIGHTReportTest.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace INSIGHTReportTest
{
    public class ReportTestBase
    {
        private INSIGHTServiceAPI _insightsericeAPI = new INSIGHTServiceAPI();
        protected INSIGHTServiceAPI INSIGHTServiceAPI
        {
            get
            {
                return _insightsericeAPI;
            }
        }

        private static string _reportoutputdir = string.Empty;
        public static string ReportOutputDir
        {
            set
            {
                _reportoutputdir = value;
            }
            get
            {
                //return _reportoutputdir;
                string folderToCheck =Config.PublicFolder;
                string absfolderToCheck = Config.LocalFolder;
                if (!Directory.Exists(folderToCheck))
                {
                    folderToCheck = absfolderToCheck;
                }
                return folderToCheck;
            }
        }
        private string _pdffilelocation = string.Empty;
        public string ReportOutputFile
        {
            get
            {
                return _pdffilelocation;
            }

            protected set
            {
                _pdffilelocation = value;
            }
        }

        public dynamic reportParameter = new ExpandoObject();
        public string ReportRequestUrl = string.Empty;


        private bool CheckReportGenerationStatus(int TaskID, ref string result)
        {
            result = string.Empty;
            Dictionary<string, int> requestbody = new Dictionary<string, int>
            {
                { "Id", TaskID }
            };
            Console.WriteLine("");
            DateTime start = DateTime.Now;
            int timeoutSeconds = 900;
            while (DateTime.Now.Subtract(start).TotalSeconds <= timeoutSeconds)
            {
                Console.Write("=");
                JObject json = INSIGHTServiceAPI.SendRequest(string.Format("{0}{1}", "/Services/BackgroundJob/Get?id=", TaskID), requestbody);


                if (json["BackgroundJob"]["Status"].ToString().ToLower() == "Warning".ToLower())
                {
                    result = json["BackgroundJob"]["Result"].ToString();
                    return false;
                }

                if (json["BackgroundJob"]["Status"].ToString().ToLower() == "success")
                {
                    Console.WriteLine("");
                    result = json["BackgroundJob"]["Result"].ToString();
                    return true;
                }

                Thread.Sleep(30000);
            }

            Console.WriteLine("CheckReportGenerationStatus() can not get the result after {0} seconds", timeoutSeconds);
            return false;
        }

        /// <summary>
        /// return full path of converted PDF
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        protected void Download(JObject json)
        {
            //Get task ID
            int TaskID = 0;
            if (Int32.TryParse(json["BackgroundJobId"].ToString(), out TaskID))
            {
                string result = string.Empty;
                if (CheckReportGenerationStatus(TaskID, ref result))
                {
                    Console.WriteLine("#####Download Report######");
                    if (string.IsNullOrEmpty(ReportOutputDir) || TaskID == 0)
                    {
                        throw new Exception();
                    }
                    ReportOutputFile = INSIGHTServiceAPI.DownloadReport(TaskID, ReportOutputDir + "\\Actual", this.GetType().Name, result);
                }
                else {
                    ReportWorker._testrail.AppendTestMessage("Failed to download file report " + this.GetType().Name);
                    if (!string.IsNullOrEmpty(result))
                    {
                        ReportWorker._testrail.AppendTestMessage(string.Format("The message: '{0}'", result));
                    }
                }
            }
           
        }
        
        
        protected int GetTransactionIDByInvoiceID(int invoiceID)
        {
            GetPolicyTransactionsRq query = new GetPolicyTransactionsRq
            {
                IsPageCountRequired = true,
                PageNumber = 1,
                RowsPerPage = 100,
                SearchCriteria = string.Format("{0}", invoiceID),
                ShowFilter = "showAll",
                SortBy = "PolicyEffectiveDate,desc;PrimaryKey,desc"
            };
            JObject json = INSIGHTServiceAPI.SendRequest(@"/services/PolicyTransaction/Search", query);

            int RecordCount = Int32.Parse(json["RecordCount"].ToString());
            int PageCount = Int32.Parse(json["RecordCount"].ToString());
            int TransID = 0;
            for (int i = 0; i < ((JArray)json["Results"]).Count; i++)
            {
                if (Int32.Parse(json["Results"][i]["InvoiceNumber"].ToString()) == invoiceID)
                {
                    TransID = Int32.Parse(json["Results"][i]["Id"].ToString());
                    return TransID;
                }
            }

            for (int i = 0; i < PageCount - 1; i++)
            {
                query = new GetPolicyTransactionsRq
                {
                    IsPageCountRequired = true,
                    PageNumber = i + 2,
                    RowsPerPage = 100,
                    SearchCriteria = string.Format("{0}", invoiceID),
                    ShowFilter = "showAll",
                    SortBy = "PolicyEffectiveDate,desc;PrimaryKey,desc"
                };
                json = INSIGHTServiceAPI.SendRequest(@"/services/PolicyTransaction/Search", query);
                for (int j = 0; j < ((JArray)json["Results"]).Count; j++)
                {
                    if (Int32.Parse(json["Results"][j]["InvoiceNumber"].ToString()) == invoiceID)
                    {
                        TransID = Int32.Parse(json["Results"][j]["Id"].ToString());
                        return TransID;
                    }
                }
            }
            return 0;
        }

        protected JObject GetPolicyTransactionDetails(int TransID)
        {
            Dictionary<string, int> requestbody = new Dictionary<string, int>
            {
                { "Id", TransID }
            };

            JObject json = INSIGHTServiceAPI.SendRequest(@"/Services/PolicyTransaction/GetEx", requestbody);

            return json;
        }

        protected JObject GetAnyRequestDetails(string endPoint, Dictionary<string, int> requestbody)
        {
            return INSIGHTServiceAPI.SendRequest(endPoint, requestbody);
        }

        public void GenerateAndDownloadReport()
        {
            ReportWorker._testrail.AppendTestMessage("#####Generate Report######");
            ReportWorker._testrail.AppendTestMessage("##### The report parameters ######"
                + Environment.NewLine
                + JsonConvert.SerializeObject(reportParameter, Formatting.Indented));
            JObject BackgroundTaskInfo = INSIGHTServiceAPI.SendRequest(ReportRequestUrl, reportParameter);

            Download(BackgroundTaskInfo);
            //await Task.Run(() => Download(BackgroundTaskInfo));
        }

        public bool IsAUMode
        {
            get
            {
                return Config.Country == "AU";
            }
        }

        protected string GetSpid(string spidUrl)
        {
            ReportWorker._testrail.AppendTestMessage("##### Get Spid ######");
            ReportWorker._testrail.AppendTestMessage(string.Format("The Spid Url: [{0}]", spidUrl));
            try
            {
                JObject res = INSIGHTServiceAPI.SendRequest(spidUrl, reportParameter);
                return res["SpId"].ToString();
            }
            catch (Exception ex)
            {
                ReportWorker._testrail.AppendTestMessage(ex.ToString());
                throw;
            }
            
            
        }
    }
}
