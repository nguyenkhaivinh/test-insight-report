@echo off


set vs2015=msbuild.exe

echo *********************
echo Insight Report Auto Test
echo *********************
.nuget\nuget.exe restore INSIGHTReportTest.sln
"%vs2015%" INSIGHTReportTest.sln /t:Clean;Build /p:Configuration=Debug /p:platform="Any CPU"
IF ERRORLEVEL 1 goto End


echo ******************************
echo Compile Finished Successfully
echo ******************************
:End
IF "%1%"=="nopause" goto Finish

pause

:Finish