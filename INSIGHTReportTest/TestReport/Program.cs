﻿using INSIGHTReportTest;
using INSIGHTReportTest.Model;
using INSIGHTReportTest.TestRail;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;

namespace TestReport
{
    class Program
    {
        public static void Main(string[] args)
        {
            string[] _args = args;
            ReportWorker rw = new ReportWorker();
            rw.Enable2TestRail = false;
            rw.CopyToBaselineIfNotExists = true;
            string testplanName = "Automation Insight Report Tests";
            rw.InitTestRailProject(testplanName, "INSIGHT", new string[] { "Regression" }, "all");

            string TestRunId = "";
            string enable2Testrail = "";
            string copybaseline = "";
            List<int> caseIds = new List<int>();
            foreach (string option in _args)
            {
                if (option.Split(':')[0] == "--testrunID")
                {
                    TestRunId = option.Split(':')[1];
                }
                else if (option.Split(':')[0] == "--Enable2TestRail")
                {
                    enable2Testrail = option.Split(':')[1];
                }
                else if (option.Split(':')[0] == "--CopyToBaselineIfNotExists")
                {
                    copybaseline = option.Split(':')[1];
                }
                else if (option.Split(':')[0] == "--CaseIds")
                {
                    string caseIdsString = option.Split(':')[1];
                    if (!string.IsNullOrEmpty(caseIdsString))
                    {
                        caseIds.AddRange(
                            caseIdsString.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                            .Select(x => Convert.ToInt32(Regex.Match(x, @"\d+").Value))
                            );
                    }
                }
            }
            if (TestRunId.Length < 1)
            {
                rw.CreateTestPlan(testplanName);
            }
            else
            {
                rw.SetTestRunIdAndGetCaseIds(Int32.Parse(TestRunId));
            }
            if (enable2Testrail.ToLower().Trim().Equals("true"))
            {
                rw.Enable2TestRail = true;
            }
            if (copybaseline.ToLower().Trim().Equals("true"))
            {
                rw.CopyToBaselineIfNotExists = true;
            }
            if (caseIds.Count > 0)
            {
                rw.SetCaseIds(caseIds);
            }

            rw.InitFolders();

            string PDF_FileType = "pdf";
            string Excel_FileType = "xls";

            rw.DownloadAndCompare(2707879, "InsurerMasterListReport", PDF_FileType);
            rw.DownloadAndCompare(2707880, "InsurerMasterListReport", Excel_FileType);

            
            rw.DownloadAndCompare(2707881, "ABNMissingReport", Excel_FileType);
            rw.DownloadAndCompare(2707882, "ABNMissingReport", PDF_FileType);
            
            rw.DownloadAndCompare(2707883, "InsurerLiabilityReport", PDF_FileType);
            rw.DownloadAndCompare(2707884, "InsurerLiabilityReport", Excel_FileType);

            rw.DownloadAndCompare(2707885, "InsurerPayablesReport", PDF_FileType);
            rw.DownloadAndCompare(2707886, "InsurerPayablesReport", Excel_FileType);

            rw.DownloadAndCompare(2707887, "InsurerStatementReport", PDF_FileType);
            rw.DownloadAndCompare(2707888, "InsurerStatementReport", Excel_FileType);

            rw.DownloadAndCompare(2707889, "APRAForm701Report", PDF_FileType);
            rw.DownloadAndCompare(2707890, "APRAForm701Report", Excel_FileType);

            rw.DownloadAndCompare(2707891, "ClientFSGEmailListReport", PDF_FileType);
            rw.DownloadAndCompare(2707892, "ClientFSGEmailListReport", Excel_FileType);

            rw.DownloadAndCompare(2707893, "FSGNotSentReport", PDF_FileType);
            rw.DownloadAndCompare(2707894, "FSGNotSentReport", Excel_FileType);

            rw.DownloadAndCompare(2707895, "SteadfastReport", PDF_FileType);
            rw.DownloadAndCompare(2707896, "SteadfastReport", Excel_FileType);

            rw.DownloadAndCompare(2707897, "AuthorisedRepLiabilityReport", PDF_FileType);
            rw.DownloadAndCompare(2707898, "AuthorisedRepLiabilityReport", Excel_FileType);

            rw.DownloadAndCompare(2707899, "AuthorisedRepPayablesReport", PDF_FileType);
            rw.DownloadAndCompare(2707900, "AuthorisedRepPayablesReport", Excel_FileType);

            rw.DownloadAndCompare(2707901, "NetAuthorisedRepStatementReport", PDF_FileType);
            rw.DownloadAndCompare(2707902, "NetAuthorisedRepStatementReport", Excel_FileType);

            rw.DownloadAndCompare(2707903, "ClaimReport", PDF_FileType);
            rw.DownloadAndCompare(2707904, "ClaimReport", Excel_FileType);

            rw.DownloadAndCompare(2707905, "ClientAnalysisByAreaReport", PDF_FileType);
            rw.DownloadAndCompare(2707906, "ClientAnalysisByAreaReport", Excel_FileType);

            rw.DownloadAndCompare(2707907, "ClientAnalysisByIndustryReport", PDF_FileType);
            rw.DownloadAndCompare(2707908, "ClientAnalysisByIndustryReport", Excel_FileType);

            rw.DownloadAndCompare(2707909, "ClientMasterListReport", PDF_FileType);
            rw.DownloadAndCompare(2707910, "ClientMasterListReport", Excel_FileType);

            rw.DownloadAndCompare(2707911, "BatchClientNotesReport", PDF_FileType);
            rw.DownloadAndCompare(2707912, "BatchClientNotesReport", Excel_FileType);

            rw.DownloadAndCompare(2707913, "IndividualClientNotesReport", PDF_FileType);
            rw.DownloadAndCompare(2707914, "IndividualClientNotesReport", Excel_FileType);

            rw.DownloadAndCompare(2707915, "ClientPermissionsGroupReport", PDF_FileType);
            rw.DownloadAndCompare(2707916, "ClientPermissionsGroupReport", Excel_FileType);

            rw.DownloadAndCompare(2707917, "ClientStatementReport", PDF_FileType);
           
            rw.DownloadAndCompare(2707918, "MailMergeClientsReport", PDF_FileType);
            rw.DownloadAndCompare(2707919, "MailMergeClientsReport", Excel_FileType);

            rw.DownloadAndCompare(2707920, "EarningsReport", PDF_FileType);
            rw.DownloadAndCompare(2707921, "EarningsReport", Excel_FileType);

            rw.DownloadAndCompare(2707922, "EarningsSummaryReport", PDF_FileType);
            rw.DownloadAndCompare(2707923, "EarningsSummaryReport", Excel_FileType);

            rw.DownloadAndCompare(2707924, "PortfolioAnalysisReport", PDF_FileType);
            rw.DownloadAndCompare(2707925, "PortfolioAnalysisReport", Excel_FileType);

            rw.DownloadAndCompare(2707926, "BusinessWrittenReport", PDF_FileType);
            rw.DownloadAndCompare(2707927, "BusinessWrittenReport", Excel_FileType);

            rw.DownloadAndCompare(2707928, "ASICReturnsReport", PDF_FileType);
            rw.DownloadAndCompare(2707929, "ASICReturnsReport", Excel_FileType);

            rw.DownloadAndCompare(2707930, "BankReconciliationReport", PDF_FileType);
            rw.DownloadAndCompare(2707931, "BankReconciliationReport", Excel_FileType);

            rw.DownloadAndCompare(2707932, "DrawingsReport", PDF_FileType);
            rw.DownloadAndCompare(2707933, "DrawingsReport", Excel_FileType);

            rw.DownloadAndCompare(2707934, "GLAccountsReport", PDF_FileType);
            rw.DownloadAndCompare(2707935, "GLAccountsReport", Excel_FileType);

            rw.DownloadAndCompare(2707936, "GLJournalReport", PDF_FileType);
            rw.DownloadAndCompare(2707937, "GLJournalReport", Excel_FileType);

            rw.DownloadAndCompare(2707938, "GeneralLedgerReport", PDF_FileType);
            rw.DownloadAndCompare(2707939, "GeneralLedgerReport", Excel_FileType);

            rw.DownloadAndCompare(2707940, "GLTransactionReport", PDF_FileType);
            rw.DownloadAndCompare(2707941, "GLTransactionReport", Excel_FileType);

            rw.DownloadAndCompare(2707942, "BusinessActivityReport", PDF_FileType);
            rw.DownloadAndCompare(2707943, "BusinessActivityReport", Excel_FileType);


            rw.DownloadAndCompare(2707944, "GSTLiabilityReport", PDF_FileType);

            rw.DownloadAndCompare(2707946, "SubLedgerAccountsReport", PDF_FileType);
            rw.DownloadAndCompare(2707947, "SubLedgerAccountsReport", Excel_FileType);

            rw.DownloadAndCompare(2707945, "TrialBalanceReport", PDF_FileType);
            rw.DownloadAndCompare(2707948, "TrialBalanceReport", Excel_FileType);

            rw.DownloadAndCompare(2707949, "IbaReconciliationReport", PDF_FileType);
            rw.DownloadAndCompare(2707950, "IbaReconciliationReport", Excel_FileType);

            rw.DownloadAndCompare(2707951, "CreditorsAgeingReport", PDF_FileType);
            rw.DownloadAndCompare(2707952, "CreditorsAgeingReport", Excel_FileType);

            rw.DownloadAndCompare(2707953, "PaymentReversalReport", PDF_FileType);
            rw.DownloadAndCompare(2707954, "PaymentReversalReport", Excel_FileType);

            rw.DownloadAndCompare(2707955, "CoinsuranceBordereauReport", PDF_FileType);
            rw.DownloadAndCompare(2707956, "CoinsuranceBordereauReport", Excel_FileType);

            rw.DownloadAndCompare(2707957, "InstalmentIncomeDueAnalysisReport", PDF_FileType);
            rw.DownloadAndCompare(2707958, "InstalmentIncomeDueAnalysisReport", Excel_FileType);

            //////// no data -> ask QC Manual
            rw.DownloadAndCompare(2707959, "InstalmentReconciliationReport", PDF_FileType);
            rw.DownloadAndCompare(2707960, "InstalmentReconciliationReport", Excel_FileType);

            rw.DownloadAndCompare(2707961, "OverseasInsurersBordereauReport", PDF_FileType);
            rw.DownloadAndCompare(2707962, "OverseasInsurersBordereauReport", Excel_FileType);

            rw.DownloadAndCompare(2707963, "PolicyMasterListReport", PDF_FileType);
            rw.DownloadAndCompare(2707964, "PolicyMasterListReport", Excel_FileType);

            rw.DownloadAndCompare(2707965, "PoliciesNotPostedReport", PDF_FileType);
            rw.DownloadAndCompare(2707966, "PoliciesNotPostedReport", Excel_FileType);

            rw.DownloadAndCompare(2707967, "ReversalPolicyReport", PDF_FileType);
            rw.DownloadAndCompare(2707968, "ReversalPolicyReport", Excel_FileType);

            rw.DownloadAndCompare(2707969, "PeriodRenewalReport", PDF_FileType);
            rw.DownloadAndCompare(2707970, "PeriodRenewalReport", Excel_FileType);

            rw.DownloadAndCompare(2707971, "RenewalRetentionReport", PDF_FileType);
            rw.DownloadAndCompare(2707972, "RenewalRetentionReport", Excel_FileType);

            rw.DownloadAndCompare(2707973, "SunrisePolicyReport", PDF_FileType);
            rw.DownloadAndCompare(2707974, "SunrisePolicyReport", Excel_FileType);

            rw.DownloadAndCompare(2707975, "SVUReport", PDF_FileType);
            rw.DownloadAndCompare(2707976, "SVUReport", Excel_FileType);

            rw.DownloadAndCompare(2707977, "CashReceiptsReport", PDF_FileType);
            rw.DownloadAndCompare(2707978, "CashReceiptsReport", Excel_FileType);

            rw.DownloadAndCompare(2707979, "DebtorsAgeingReport", PDF_FileType);
            rw.DownloadAndCompare(2707980, "DebtorsAgeingReport", Excel_FileType);

            rw.DownloadAndCompare(2707981, "InstalmentPaymentReceiptReport", PDF_FileType);
            rw.DownloadAndCompare(2707982, "InstalmentPaymentReceiptReport", Excel_FileType);

            rw.DownloadAndCompare(2707983, "PremiumFundingReport", PDF_FileType);
            rw.DownloadAndCompare(2707984, "PremiumFundingReport", Excel_FileType);

            rw.DownloadAndCompare(2707985, "ReceiptsAnalysisReport", PDF_FileType);
            rw.DownloadAndCompare(2707986, "ReceiptsAnalysisReport", Excel_FileType);

            rw.DownloadAndCompare(2707987, "UnallocatedCashCreditsReport", PDF_FileType);
            rw.DownloadAndCompare(2707988, "UnallocatedCashCreditsReport", Excel_FileType);

            rw.DownloadAndCompare(2707989, "UnallocatedCashReturnsReport", PDF_FileType);
            rw.DownloadAndCompare(2707990, "UnallocatedCashReturnsReport", Excel_FileType);

            ////// slow download pdf
            rw.DownloadAndCompare(2707991, "DiaryReminderReport", PDF_FileType);
            rw.DownloadAndCompare(2707992, "DiaryReminderReport", Excel_FileType);
            

            rw.DownloadAndCompare(2707993, "TaskManagementReport", PDF_FileType);
            rw.DownloadAndCompare(2707994, "TaskManagementReport", Excel_FileType);

            rw.DownloadAndCompare(2707995, "EventPublicationResyncReport", PDF_FileType);
            rw.DownloadAndCompare(2707996, "EventPublicationResyncReport", Excel_FileType);

            rw.DownloadAndCompare(2707997, "ProcessingPermissionGroupsReport", Excel_FileType);
            //*/

            
            //rw.CloseTestPlan();
            Console.WriteLine("DONE");
        }


    }
}
